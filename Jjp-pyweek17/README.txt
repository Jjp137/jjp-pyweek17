The Lunar Project
===============

Entry in PyWeek #17  <http://www.pyweek.org/17/>
URL: http://pyweek.org/e/Jjp-pyweek17/
Team: Jjp-pyweek17
Members: Jjp137
License: see LICENSE.txt for full details

Dependencies
------------

This game requires Python 2.7 and PyGame 1.9.1 to run.

It is untested on Python 3 and may not work. Older versions of Python 2 may
not work as well.

Running the Game
----------------

On Windows or Mac OS X, locate the "run_game.pyw" file and double-click it.

Otherwise open a terminal / console and "cd" to the game directory and run:

  python run_game.py

There are no command line parameters.

How to Play the Game
--------------------

More information is in MANUAL.txt, but here is a control reference:

1 - Select drill
2 - Select scanner
3 - Select machine gun
4 - Select spread gun
5 - Select missiles
W,A,S,D or arrow keys - Move**
Mouse button - Fire / Use equipped tool
Spacebar - Use teleporter / Return to map after dying
Escape - Pause the game

Basically, you want to use the scanner to reveal areas that you can use your
drill on. Obtain coordinate data to unlock new areas, and obtain blueprints
to unlock new weapons in the shop. Use the teleporter to leave areas.

Aiming weapons is done with the mouse. The bullets or missiles will travel
towards your mouse pointer. Defending yourself is important.

Menus are also navigated with the mouse. Click to select options.

**Technical note: if you have a different keyboard layout and you don't want 
to use the arrow keys, you may edit game/game_event.py to reassign the controls. 
I couldn't get a control configuration screen done :(

Credits
-------

Jjp137 wrote the code for the game, designed the levels, and made some of
the graphics. 

data.py and this README template was taken from Skellington 2.3.

The sound effects were generated from the Bfxr tool, which can be found here:
http://www.bfxr.net/

The graphics for the player's ship were taken from:
http://opengameart.org/content/spaceship-360
Credit goes to Clint Bellanger for these as well as Killy Overdrive for the
original model.

The Xolonium font used in the game was taken from:
http://openfontlibrary.org/en/font/xolonium

The gem graphics (which went unused, sadly) were taken from:
http://opengameart.org/content/basic-gems-icon-set-remix
Credit goes to prdatur for these graphics.

The images used as the base for the enemies were taken from:
http://opengameart.org/content/spaceship-set-32x32px
Credit goes to Scrittl for these graphics.

The rock image used in the game was taken from:
http://opengameart.org/content/grey-rock
Credit goes to Atrus for this graphic.

The moon image used in the background of the menus was taken from:
http://opengameart.org/content/the-moon
Credit goes to JosipKladaric for this graphic.

See LICENSE.txt for licensing information.

Special Thanks
--------------

First of all, special thanks goes to those in charge of PyWeek. Thank you
for hosting this event :)

Thanks to everyone else that participated as well. The diary entries and
the #pyweek IRC channel were fun to read. :)

Also, special thanks goes to my friends for supporting me throughout the
years and for giving me several ideas :)

...and finally, thank you for playing! :D

- Jjp137

