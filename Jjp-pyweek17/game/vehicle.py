import pygame
from pygame.locals import *

import data
import equipment
import util
from constants import *

class Vehicle(object):
    def __init__(self, start_pos, profile):
        self.pos = start_pos[:]
        self.profile = profile
        
        self.up = False
        self.down = False
        self.left = False
        self.right = False
        self.frozen = False
        self.dead = False
        
        # All images
        self.north_image = util.load_alpha_image("ship-n.png")
        self.northeast_image = util.load_alpha_image("ship-ne.png")
        self.east_image = util.load_alpha_image("ship-e.png")
        self.southeast_image = util.load_alpha_image("ship-se.png")
        self.south_image = util.load_alpha_image("ship-s.png")
        self.southwest_image = util.load_alpha_image("ship-sw.png")
        self.west_image = util.load_alpha_image("ship-w.png")
        self.northwest_image = util.load_alpha_image("ship-nw.png")
        
        # All sounds
        self.gem_sound = pygame.mixer.Sound(data.filepath("gem-pickup.wav"))
        self.powerup_sound = pygame.mixer.Sound(data.filepath("powerup.wav"))
        self.shoot_sound = pygame.mixer.Sound(data.filepath("player-shoot.wav"))
        self.damage_sound = pygame.mixer.Sound(data.filepath("player-hurt.wav"))
        self.death_sound = pygame.mixer.Sound(data.filepath("player-death.wav"))
        self.missile_sound = pygame.mixer.Sound(data.filepath("player-missile.wav"))
        self.scanner_sound = pygame.mixer.Sound(data.filepath("scanner.wav"))
        
        self.gem_sound_play = False
        self.powerup_sound_play = False
        self.shoot_sound_play = False
        self.damage_sound_play = False
        self.death_sound_play = False
        self.missile_sound_play = False
        self.scanner_sound_play = False
        
        # For when the player is not moving...
        self.last_image = self.north_image
        
        self.current_health = self.profile.current_health
        self.max_health = self.profile.max_health
        self.current_fuel = self.profile.current_fuel
        self.max_fuel = self.profile.max_fuel
        self.money = self.profile.money
        self.speed = self.profile.speed
        
        self.fuel_timer = 4 * FPS
        
        self.key_inventory = []
        
        self.equipment_actions = {EQUIP_ONE : equipment.Drill(self.profile.drill_level),
                                  EQUIP_TWO : equipment.Scanner(self.profile.scanner_level),
                                  EQUIP_THREE : equipment.MachineGun(self.profile.machine_gun_level),
                                  EQUIP_FOUR : equipment.SpreadGun(self.profile.spread_level),
                                  EQUIP_FIVE : equipment.Missiles(self.profile.missile_level, 
                                    self.profile.missile_count)}
        self.current_equip = self.equipment_actions[EQUIP_ONE]
        
        self.equip_ids = {EQUIP_ONE : DRILL, 
                          EQUIP_TWO : SCANNER, 
                          EQUIP_THREE : MACHINE_GUN,
                          EQUIP_FOUR : SPREAD_GUN,
                          EQUIP_FIVE : MISSILE}
        self.current_equip_id = DRILL
        
        self.gem_dict = {GREEN_GEM : 10, BLUE_GEM : 50, YELLOW_GEM : 200,
            RED_GEM : 500, PINK_GEM : 1000}
            
    def read_input(self, event):
        if event.action == EVENT_IGNORE or self.current_health <= 0:
            return
            
        if event.action == EQUIP_ACTION and self.current_fuel > 0:
            self.current_equip.read_input(self, event)
            return
        
        if event.action == MOVE_UP:
            self.up = True if event.button_state == PRESSED else False
            
        if event.action == MOVE_DOWN:
            self.down = True if event.button_state == PRESSED else False
            
        if event.action == MOVE_LEFT:
            self.left = True if event.button_state == PRESSED else False
        
        if event.action == MOVE_RIGHT:
            self.right = True if event.button_state == PRESSED else False
            
        if self.valid_equip_switch(event):
            self.current_equip.on_inactive_equip(self)
            self.current_equip = self.equipment_actions[event.action]
            self.current_equip_id = self.equip_ids[event.action]
            self.current_equip.on_active_equip(self)
    
    def valid_equip_switch(self, event):
        return event.button_state == RELEASED \
        and event.action in self.equipment_actions.keys() \
        and self.equip_ids[event.action] != self.current_equip_id \
        and self.equipment_actions[event.action].level != 0
        
    def update_pos(self, terrain):
        if self.frozen:
            return
        
        x_move = 0
        y_move = 0
        
        if self.current_fuel == 0:
            speed = self.speed / 2
        else:
            speed = self.speed
        
        if self.up:
            y_move -= speed

        if self.down:
            y_move += speed
            
        if self.left:
            x_move -= speed
            
        if self.right:
            x_move += speed
            
        x_collision = False
        y_collision = False
        
        x_shift = [self.pos[X_INDEX] + x_move, self.pos[Y_INDEX]]
        while terrain.check_immobile_collision(self.get_pos_rect(x_shift)):
            x_collision = True
            
            if x_move > 0:
                x_move -= 1
                x_shift[X_INDEX] -= 1
            elif x_move < 0:
                x_move += 1
                x_shift[X_INDEX] += 1
        
        y_shift = [self.pos[X_INDEX], self.pos[Y_INDEX] + y_move]
        while terrain.check_immobile_collision(self.get_pos_rect(y_shift)):
            y_collision = True
            
            if y_move > 0:
                y_move -= 1
                y_shift[Y_INDEX] -= 1
            elif y_move < 0:
                y_move += 1
                y_shift[Y_INDEX] += 1
                
        # Check for both at the same time here
        both_shift = [self.pos[X_INDEX] + x_move, self.pos[Y_INDEX] + y_move]
        if not x_collision and not y_collision:
            while terrain.check_immobile_collision(self.get_pos_rect(both_shift)):
                if x_move > 0:
                    x_move -= 1
                    both_shift[X_INDEX] -= 1
                elif x_move < 0:
                    x_move += 1
                    both_shift[X_INDEX] += 1
                
                if y_move > 0:
                    y_move -= 1
                    both_shift[Y_INDEX] -= 1
                elif y_move < 0:
                    y_move += 1
                    both_shift[Y_INDEX] += 1
        
        self.pos[X_INDEX] += x_move
        self.pos[Y_INDEX] += y_move
        
    def update_equip(self, terrain):
        self.fuel_timer -= 1
        if self.fuel_timer <= 0 and self.current_fuel > 0:
            self.current_fuel -= 1
            self.fuel_timer = 4 * FPS
            
            if self.current_fuel <= 0:
                self.current_equip.on_no_fuel(self)
        
        self.current_equip.update(self, terrain)
        
    def on_damage(self, damage):
        self.current_health -= damage
        
        if self.current_health <= 0:
            self.current_health = 0
            self.frozen = True
            self.current_equip.on_inactive_equip(self)
            
            self.up = False
            self.down = False
            self.left = False
            self.right = False
            
            if not self.dead:
                self.death_sound_play = True
                self.dead = True

        else:
            self.damage_sound_play = True
            
    def apply_item(self, item):
        if item in self.gem_dict.keys():
            self.money += self.gem_dict[item]
            self.gem_sound_play = True
        elif item == HEALTH_CELL:
            self.current_health += 5
            self.powerup_sound_play = True
        elif item == LARGE_HEALTH_CELL:
            self.current_health += 20
            self.powerup_sound_play = True
        elif item == FUEL_CELL:
            self.current_fuel += 10
            self.powerup_sound_play = True
        elif item == LARGE_FUEL_CELL:
            self.current_fuel += 40
            self.powerup_sound_play = True
            
        elif item in self.profile.key_items.keys():
            self.key_inventory.append(item)
            self.powerup_sound_play = True
        
        if self.current_health > self.max_health:
            self.current_health = self.max_health
        elif self.current_fuel > self.max_fuel:
            self.current_fuel = self.max_fuel
            
    def play_sound(self):
        if self.gem_sound_play:
            self.gem_sound.play()
        if self.powerup_sound_play:
            self.powerup_sound.play()
        if self.shoot_sound_play:
            self.shoot_sound.play()
        if self.damage_sound_play:
            self.damage_sound.play()
        if self.death_sound_play:
            self.death_sound.play()
        if self.missile_sound_play:
            self.missile_sound.play()
        if self.scanner_sound_play:
            self.scanner_sound.play()
        
    def draw(self, canvas, draw_pos=None):
        # Reset all sounds (idk why here, though)
        self.gem_sound_play = False
        self.powerup_sound_play = False
        self.shoot_sound_play = False
        self.damage_sound_play = False
        self.death_sound_play = False
        self.missile_sound_play = False
        self.drill_sound_play = False
        self.scanner_sound_play = False
        
        if self.current_health <= 0:
            return
        
        if draw_pos == None:
            draw_pos = self.pos
            print "Warning: No draw_pos given."
        
        image_to_draw = self.get_facing_image()
        canvas.blit(image_to_draw, draw_pos)
        self.last_image = image_to_draw
        
    def get_facing_image(self):
        if self.frozen:
            return self.last_image
        
        if self.up and self.left and not self.down and not self.right:
            return self.northwest_image
        elif self.up and self.right and not self.down and not self.left:
            return self.northeast_image
        elif self.down and self.left and not self.up and not self.right:
            return self.southwest_image
        elif self.down and self.right and not self.up and not self.left:
            return self.southeast_image
        
        elif self.up and not self.down:
            return self.north_image
        elif self.down and not self.up:
            return self.south_image
        elif self.left and not self.right:
            return self.west_image
        elif self.right and not self.left:
            return self.east_image
            
        else:
            return self.last_image
    
    def get_pos_rect(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        return pygame.Rect(pos[X_INDEX], pos[Y_INDEX], PLAYERSIZE, PLAYERSIZE)
        
    def get_hitbox(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        hitbox = pygame.Rect(pos[X_INDEX], pos[Y_INDEX], 16, 16)
        hitbox.center = self.get_pos_rect(pos).center
        
        return hitbox
