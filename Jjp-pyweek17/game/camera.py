import pygame
from pygame.locals import *

from constants import *

class Camera(object):
    def __init__(self, level):
        self.pos = [0, 0]
        
        self.level = level
        self.vehicle = level.player_vehicle

    def update_pos(self):
        vehicle_draw_pos = self.get_draw_pos(self.vehicle)
        level_size = self.level.level_size
        
        if vehicle_draw_pos[X_INDEX] < LEFT_CAM_EDGE and self.pos[X_INDEX] > 0:
            self.pos[X_INDEX] -= int(LEFT_CAM_EDGE - vehicle_draw_pos[X_INDEX])
            
        if vehicle_draw_pos[X_INDEX] > RIGHT_CAM_EDGE and \
            self.pos[X_INDEX] + WINDOW_WIDTH < level_size[X_INDEX]:
            self.pos[X_INDEX] += int(vehicle_draw_pos[X_INDEX] - RIGHT_CAM_EDGE)
            
        if vehicle_draw_pos[Y_INDEX] < UP_CAM_EDGE and self.pos[Y_INDEX] > 0:
            self.pos[Y_INDEX] -= int(UP_CAM_EDGE - vehicle_draw_pos[Y_INDEX])
            
        if vehicle_draw_pos[Y_INDEX] > DOWN_CAM_EDGE and \
            self.pos[Y_INDEX] + WINDOW_HEIGHT - STATUS_HEIGHT < level_size[Y_INDEX]:
            self.pos[Y_INDEX] += int(vehicle_draw_pos[Y_INDEX] - DOWN_CAM_EDGE)
            
    def draw_field(self, canvas):
        moon_tile = self.level.tile_image
        tile_origin = [0, 0]
        
        tile_origin[X_INDEX] = -(self.pos[X_INDEX] % TILESIZE)
        tile_origin[Y_INDEX] = -(self.pos[Y_INDEX] % TILESIZE)
        
        for x_value in xrange(tile_origin[X_INDEX], WINDOW_WIDTH, TILESIZE):
            for y_value in xrange(tile_origin[Y_INDEX], WINDOW_HEIGHT, TILESIZE):
                canvas.blit(moon_tile, (x_value, y_value))
        
        self.level.teleporter.draw(canvas, self.get_draw_pos(self.level.teleporter))
        
        for barrier in self.level.barriers:
            barrier.draw(canvas, self.get_draw_pos(barrier))
        
        for site in self.level.dig_sites:
            site.draw(canvas, self.get_draw_pos(site))
            
        for bullet in self.level.bullets:
            bullet.draw(canvas, self.get_draw_pos(bullet))
            
        self.vehicle.draw(canvas, self.get_draw_pos(self.vehicle))
         
        for enemy in self.level.enemies:
            enemy.draw(canvas, self.get_draw_pos(enemy))
    
    def get_draw_pos(self, entity):
        return [entity.pos[X_INDEX] - self.pos[X_INDEX],
                entity.pos[Y_INDEX] - self.pos[Y_INDEX]]
                
    def get_world_pos(self, draw_pos):
        return (draw_pos[X_INDEX] + self.pos[X_INDEX],
                draw_pos[Y_INDEX] + self.pos[Y_INDEX])
