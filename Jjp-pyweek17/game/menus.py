import pygame
import sys
from pygame.locals import *

import screens
import util
import data
from constants import *

class Button(object):
    def __init__(self, pos, size, text, font_size, callback):
        self.pos = pos
        self.size = size
        self.text = text
        self.font_size = font_size
        self.callback = callback
        #self.selectable = True
        
        self.hover = False
        self.clicked = False
        
        self.font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), self.font_size)
        
    def read_pointer(self, pointer):
        if self.get_rect_pos().collidepoint(pointer):
            self.hover = True
        else:
            self.hover = False
        
    def read_input(self, event):
        if event.type == MOUSEBUTTONDOWN and event.button == 1 and self.hover:
            self.clicked = True
        if event.type == MOUSEBUTTONUP and event.button == 1 \
            and self.hover and self.clicked:
            self.callback()
            self.clicked = False
        
    def draw(self, canvas):
        button_rect = self.get_rect_pos()
        pygame.draw.rect(canvas, BLACK, button_rect)
        
        outline_color = YELLOW if self.hover else WHITE
        pygame.draw.rect(canvas, outline_color, button_rect, 5)
        
        self.draw_centered_words(canvas)
        
    def get_rect_pos(self):
        return pygame.Rect(self.pos[X_INDEX], self.pos[Y_INDEX], 
            self.size[X_INDEX], self.size[Y_INDEX])
            
    def draw_centered_words(self, canvas):
        msg_surf = self.font.render(self.text, True, WHITE)
        msg_rect = msg_surf.get_rect()
        msg_rect.center = self.get_rect_pos().center
        canvas.blit(msg_surf, msg_rect)


class MainMenu(object):
    def __init__(self, screen):
        self.buttons = []
        self.create_buttons()
        
        self.screen = screen
        self.moon_pic = util.load_alpha_image("the-moon.png")
        self.small_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        
    def create_buttons(self):
        start_game_pos = (200, 300)
        start_game_size = (400, 50)
        start_game_button = Button(start_game_pos, start_game_size, 
            "Start Game", 32, self.begin_game)
        
        options_pos = (200, 370)
        options_size = (400, 50)
        options_button = Button(options_pos, options_size, 
            "Options", 32, self.open_options)
        
        exit_game_pos = (200, 440)
        exit_game_size = (400, 50)
        exit_game_button = Button(exit_game_pos, exit_game_size, 
            "Quit Game", 32, self.end_game)
            
        self.buttons.append(start_game_button)
        self.buttons.append(options_button)
        self.buttons.append(exit_game_button)
    
    def read_pointer(self, pointer):
        for button in self.buttons:
            button.read_pointer(pointer)
        
    def read_input(self, event):
        for button in self.buttons:
            button.read_input(event)
    
    def draw(self, canvas):
        moon_pos = (144, 69)
        canvas.blit(self.moon_pic, moon_pos)
        
        for button in self.buttons:
            button.draw(canvas)
        
        help_string = "Click on a menu option to select it."
        msg_surf = self.small_font.render(help_string, True, WHITE)
        msg_rect = msg_surf.get_rect()
        msg_rect.topleft = (10, 620)
        canvas.blit(msg_surf, msg_rect)

    def begin_game(self):
        cur_screen = self.screen
        #cur_screen.screen_stack.clear_stack()
        
        new_screen = screens.GameplayScreen(cur_screen.screen_stack,
            cur_screen.canvas, cur_screen.clock, cur_screen.profile, "site2.txt")
        
        cur_screen.screen_stack.add_screen(new_screen)
        
    def open_options(self):
        cur_screen = self.screen
        new_screen = screens.OptionsScreen(cur_screen.screen_stack,
            cur_screen.canvas, cur_screen.clock, cur_screen.profile)
        
        cur_screen.screen_stack.add_screen(new_screen)
        
    def end_game(self):
        pygame.quit()
        sys.exit()
