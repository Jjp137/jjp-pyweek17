import pygame
import random
from pygame.locals import *

import data
import entities
import enemies
import vehicle
from constants import *

class Terrain(object):
    def __init__(self, level_file, profile):
        self.level_id = None
        self.level_size = None
        self.start_pos = None
        self.teleporter = None
        self.player_vehicle = None
        self.profile = profile
        self.screen_flag = None
        
        self.barriers = []
        self.dig_sites = []
        self.bullets = []
        self.enemies = []
        self.special_flags = {}
        
        self.tile_image = None
        self.msg_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        self.msg_queue = []
        
        self.teleporter_sound = pygame.mixer.Sound(data.filepath("powerup.wav"))
        self.enemy_hit_sound = pygame.mixer.Sound(data.filepath("enemy-hurt.wav"))
        self.enemy_shoot_sound = pygame.mixer.Sound(data.filepath("enemy-shoot.wav"))
        self.enemy_death_sound = pygame.mixer.Sound(data.filepath("enemy-death.wav"))
        self.boss_wakeup_sound = pygame.mixer.Sound(data.filepath("boss-wakeup.wav"))
        self.boss_death_sound = pygame.mixer.Sound(data.filepath("boss-death.wav"))
        
        self.enemy_hit_sound_play = False
        self.enemy_shoot_sound_play = False
        self.enemy_death_sound_play = False
        
        self.item_table = {GREEN_GEM : ["Obtained a green gem. [+10 money]", 3 * FPS],
                           BLUE_GEM : ["Obtained a blue gem. [+50 money]", 3 * FPS],
                           YELLOW_GEM : ["Obtained a yellow gem. [+200 money]", 3 * FPS],
                           RED_GEM : ["Obtained a red gem. [+500 money]", 3 * FPS],
                           PINK_GEM : ["Obtained a pink gem! [+1000 money]", 3 * FPS],
                           HEALTH_CELL : ["Obtained a health cell. [+5 health]", 3 * FPS],
                           FUEL_CELL : ["Obtained a fuel cell. [+10 fuel]", 3 * FPS],
                           LARGE_HEALTH_CELL : ["Obtained a large health cell. [+20 health]", 3 * FPS],
                           LARGE_FUEL_CELL : ["Obtained a large fuel cell. [+40 fuel]", 3 * FPS],
                           A2_COORDS : ["Obtained coordinate data for Alpha Site 2.", 5 * FPS],
                           A3_COORDS : ["Obtained coordinate data for Alpha Site 3.", 5 * FPS],
                           B1_COORDS : ["Obtained coordinate data for Beta Site 1.", 5 * FPS],
                           B2_COORDS : ["Obtained coordinate data for Beta Site 2.", 5 * FPS],
                           B3_COORDS : ["Obtained coordinate data for Beta Site 3.", 5 * FPS],
                           B4_COORDS : ["Obtained coordinate data for Beta Site 4.", 5 * FPS],
                           OM1_COORDS : ["Obtained coordinate data for Omega Site 1.", 5 * FPS],
                           OM2_COORDS_A : ["Obtained part 1 of 4 of some coordinate data...?", 5 * FPS],
                           OM2_COORDS_B : ["Obtained part 2 of 4 of some coordinate data...?", 5 * FPS],
                           OM2_COORDS_C : ["Obtained part 3 of 4 of some coordinate data...?", 5 * FPS],
                           OM2_COORDS_D : ["Obtained part 4 of 4 of some coordinate data...?", 5 * FPS],
                           RED_CRYSTAL : ["Obtained a mysterious red crystal...", 5 * FPS],
                           GUN_SCHEME : ["Obtained blueprints for a machine gun.", 5 * FPS],
                           SPREAD_SCHEME : ["Obtained blueprints for a spread gun.", 5 * FPS],
                           MISSILE_SCHEME : ["Obtained blueprints for missiles.", 5 * FPS]}

        self.interpret_file(level_file)
        level_file.close()
        
        self.init_special_behavior()
        
    def interpret_file(self, level_file):
        for line in level_file:
            if line[0] == '#':
                continue
            
            split_line = line.strip().split(':')
            
            if split_line[0] == "name":
                self.level_id = split_line[1]
            elif split_line[0] == "bg":
                self.pick_bg(split_line[1])
            
            elif split_line[0] == "size":
                size_data = split_line[1]
                self.level_size = self.interpret_coord_line(size_data)
                self.add_border()
            elif split_line[0] == "teleporter":
                start_pos_data = split_line[1]
                self.start_pos = self.interpret_coord_line(start_pos_data)
                self.player_vehicle = vehicle.Vehicle(self.start_pos, self.profile)
                self.teleporter = entities.Teleporter(self.start_pos)
                
            elif split_line[0] == "horiz-wall":
                wall_data = split_line[1]
                self.add_wall(HORIZONTAL, wall_data)
            elif split_line[0] == "vert-wall":
                wall_data = split_line[1]
                self.add_wall(VERTICAL, wall_data)
                
            elif split_line[0] == "green":
                pos_data = split_line[1].split(',')
                enemy_pos = [int(pos_data[X_INDEX]), int(pos_data[Y_INDEX])]
                self.enemies.append(enemies.GreenThing(enemy_pos))
            elif split_line[0] == "strong-green":
                pos_data = split_line[1].split(',')
                enemy_pos = [int(pos_data[X_INDEX]), int(pos_data[Y_INDEX])]
                self.enemies.append(enemies.StrongGreenThing(enemy_pos))
            elif split_line[0] == "yellow":
                pos_data = split_line[1].split(',')
                enemy_pos = [int(pos_data[X_INDEX]), int(pos_data[Y_INDEX])]
                self.enemies.append(enemies.YellowThing(enemy_pos))
            elif split_line[0] == "blue":
                pos_data = split_line[1].split(',')
                enemy_pos = [int(pos_data[X_INDEX]), int(pos_data[Y_INDEX])]
                self.enemies.append(enemies.BlueThing(enemy_pos))
            elif split_line[0] == "pink":
                pos_data = split_line[1].split(',')
                enemy_pos = [int(pos_data[X_INDEX]), int(pos_data[Y_INDEX])]
                self.enemies.append(enemies.PinkThing(enemy_pos))
            elif split_line[0] == "black":
                pos_data = split_line[1].split(',')
                enemy_pos = [int(pos_data[X_INDEX]), int(pos_data[Y_INDEX])]
                self.enemies.append(enemies.BlackThing(enemy_pos))
            
            elif split_line[0] == "key-item":
                site_data = split_line[1].split(',')
                item_pos = [int(site_data[X_INDEX]), int(site_data[Y_INDEX])]
                item_num = int(site_data[2])
                
                if not self.profile.key_items[item_num]:
                    item_site = entities.DigSite(item_pos, item_num)
                    item_site.key_item = True
                    self.dig_sites.append(item_site)
                
            elif split_line[0] == "rocks":
                rock_num = int(split_line[1])
                self.add_rocks(rock_num)
            elif split_line[0] == "gems":
                gem_data = split_line[1]
                self.add_gems(gem_data)

            elif split_line[0] == "healthcell":
                health_data = split_line[1]
                self.add_health_pickups(health_data)
            elif split_line[0] == "fuelcell":
                fuel_data = split_line[1]
                self.add_fuel_pickups(fuel_data)
                
    def pick_bg(self, bg):
        if bg == "moon":
            self.tile_image = pygame.image.load(data.filepath("moon-tile.png")).convert()
        elif bg == "tinted-moon":
            self.tile_image = pygame.image.load(data.filepath("tinted-moon-tile.png")).convert()
        elif bg == "corrupt-moon":
            self.tile_image = pygame.image.load(data.filepath("corrupt-moon-tile.png")).convert()
        else:
            raise ValueError
    
    def interpret_coord_line(self, data):
        coords = data.split(',')
        coords[X_INDEX] = int(coords[X_INDEX])
        coords[Y_INDEX] = int(coords[Y_INDEX])
        
        return [coords[X_INDEX], coords[Y_INDEX]]
        
    def add_wall(self, orientation, wall_data):
        split_walls = wall_data.split(',')
        start_x = int(split_walls[0])
        start_y = int(split_walls[1])
        length = int(split_walls[2])
        
        barrier_pos = [start_x, start_y]
        
        if orientation == HORIZONTAL:
            self.barriers.append(entities.Barrier(HORIZONTAL, barrier_pos, length))
        elif orientation == VERTICAL:
            self.barriers.append(entities.Barrier(VERTICAL, barrier_pos, length))
        else:
            raise ValueError
        
    def add_rocks(self, num):
        if num <= 0:
            return
        
        current_num = 1
        while current_num <= num:
            x_coord = random.randint(0, self.level_size[X_INDEX] - 25)
            y_coord = random.randint(0, self.level_size[Y_INDEX] - 25)
            proposed_rect = pygame.Rect(x_coord, y_coord, ROCKSIZE, ROCKSIZE)
            
            if self.okay_to_place(proposed_rect):
                self.barriers.append(entities.Rock((x_coord, y_coord)))
                current_num += 1
                
    def add_gems(self, nums):
        gem_array = [GREEN_GEM, BLUE_GEM, YELLOW_GEM, RED_GEM, PINK_GEM]
        gem_counts = nums.split(',')
        
        for gem_index, gem_num in enumerate(gem_counts):
            self.add_dig_sites(int(gem_num), gem_array[gem_index])
            
    def add_health_pickups(self, nums):
        split_nums = nums.split(',')
        small_count = int(split_nums[0])
        large_count = int(split_nums[1])
        
        self.add_dig_sites(small_count, HEALTH_CELL)
        self.add_dig_sites(large_count, LARGE_HEALTH_CELL)
        
    def add_fuel_pickups(self, nums):
        split_nums = nums.split(',')
        small_count = int(split_nums[0])
        large_count = int(split_nums[1])
        
        self.add_dig_sites(small_count, FUEL_CELL)
        self.add_dig_sites(large_count, LARGE_FUEL_CELL)
    
    def add_dig_sites(self, num, item):
        if num <= 0:
            return
        
        current_num = 1
        while current_num <= num:
            x_coord = random.randint(0, self.level_size[X_INDEX] - 25)
            y_coord = random.randint(0, self.level_size[Y_INDEX] - 25)
            proposed_rect = pygame.Rect(x_coord, y_coord, DIGSIZE, DIGSIZE)
            
            if self.okay_to_place(proposed_rect):
                new_site = entities.DigSite((x_coord, y_coord), item)
                self.dig_sites.append(new_site)
                current_num += 1
    
    def okay_to_place(self, rect):
        return not self.check_immobile_collision(rect) and \
            not self.check_site_collision(rect) and \
            not self.check_enemy_collision(rect) and \
            not self.teleporter.get_pos_rect().colliderect(rect)
    
    def add_border(self):
        top_barrier_pos = [0, 0]
        bottom_barrier_pos = [0, self.level_size[Y_INDEX] - TILESIZE]
            
        self.barriers.append(entities.Barrier(HORIZONTAL, top_barrier_pos, self.level_size[X_INDEX]))
        self.barriers.append(entities.Barrier(HORIZONTAL, bottom_barrier_pos, self.level_size[X_INDEX]))
        
        left_barrier_pos = [0, 0]
        right_barrier_pos = [self.level_size[X_INDEX] - TILESIZE, 0]
            
        self.barriers.append(entities.Barrier(VERTICAL, left_barrier_pos, self.level_size[Y_INDEX]))
        self.barriers.append(entities.Barrier(VERTICAL, right_barrier_pos, self.level_size[Y_INDEX]))
        
    def init_special_behavior(self):
        if self.level_id == "omega-2":
            self.special_flags["boss-start"] = False
            self.special_flags["boss-finish"] = False
            
    def read_input(self, event):
        if event.action == OTHER_ACTION and event.button_state == RELEASED:
            if self.player_vehicle.current_health <= 0:
                self.screen_flag = DEAD
            elif self.check_tele_collision(self.player_vehicle.get_pos_rect()):
                self.screen_flag = FINISHED
                self.update_profile()
                
                if self.profile.sound:
                    self.teleporter_sound.play()
            
        self.player_vehicle.read_input(event)
        
    def read_pointer(self, mouse_world_pos):
        self.player_vehicle.current_equip.store_pointer(mouse_world_pos)
    
    def update(self):
        self.player_vehicle.update_pos(self)
        self.player_vehicle.update_equip(self)
        self.update_special()
        
        for site in self.dig_sites:
            site.update()
        
        for enemy in self.enemies:
            enemy.update(self.player_vehicle, self)
            
        self.process_bullets()
        self.clean_bullets()
        self.clean_enemies()
        
    def update_profile(self):
        self.profile.update(self.player_vehicle)
        
        if self.check_key_items():
            self.profile.location_state[self.level_id] = NOT_COMPLETE
        else:
            self.profile.location_state[self.level_id] = COMPLETE
            
        self.profile.save()
            
    def update_special(self):
        if self.level_id == "omega-2":
            if self.profile.key_items[RED_CRYSTAL]:
                return
                
            elif not self.special_flags["boss-start"] and not self.check_key_items():
                self.barriers.append(entities.Barrier(HORIZONTAL, (400, 875), 450))
                self.special_flags["boss-start"] = True
                self.enemies.append(enemies.Boss([344, 950]))
                
                if self.profile.sound:
                    self.boss_wakeup_sound.play()
            
            elif self.special_flags["boss-start"] and len(self.enemies) == 0 \
                and not self.special_flags["boss-finish"]:
                self.special_flags["boss-finish"] = True
                del self.barriers[-1]
                
                if self.profile.sound:
                    self.boss_death_sound.play()
                
    def play_sound(self):
        self.player_vehicle.play_sound()
        
        if self.enemy_hit_sound_play:
            self.enemy_hit_sound.play()
        if self.enemy_shoot_sound_play:
            self.enemy_shoot_sound.play()
        if self.enemy_death_sound_play:
            self.enemy_death_sound.play()
        
    def draw_messages(self, canvas):
        # Handle sounds here, for some reason
        self.enemy_hit_sound_play = False
        self.enemy_shoot_sound_play = False
        self.enemy_death_sound_play = False
        
        first_msg = None
        
        if self.check_tele_collision(self.player_vehicle.get_pos_rect()):
            first_msg = "Press SPACE to exit the area."
        elif self.player_vehicle.current_health <= 0:
            first_msg = "You have died. Press SPACE to return to the map."
        
        if first_msg != None:
            first_msg_surf = self.msg_font.render(first_msg, True, WHITE)
            first_msg_rect = first_msg_surf.get_rect()
            first_msg_rect.topleft = (10, 490)
            
            first_msg_bg_size = (first_msg_rect.width + 25, 25)
            first_msg_bg = pygame.Surface(first_msg_bg_size).convert_alpha()
            first_msg_bg.fill(TRANS_BLACK)
            first_msg_bg_rect = first_msg_bg.get_rect()
            first_msg_bg_rect.topleft = (0, 485)
            
            canvas.blit(first_msg_bg, first_msg_bg_rect)
            canvas.blit(first_msg_surf, first_msg_rect)
            
        if len(self.msg_queue) > 0:
            msg_data = self.msg_queue[0]
            queue_msg = msg_data[0]
            
            queue_msg_surf = self.msg_font.render(queue_msg, True, WHITE)
            queue_msg_rect = queue_msg_surf.get_rect()
            queue_msg_rect.topleft = (10, 520)
            
            queue_msg_bg_size = (queue_msg_rect.width + 25, 25)
            queue_msg_bg = pygame.Surface(queue_msg_bg_size).convert_alpha()
            queue_msg_bg.fill(TRANS_BLACK)
            queue_msg_bg_rect = queue_msg_bg.get_rect()
            queue_msg_bg_rect.topleft = (0, 515)
            
            canvas.blit(queue_msg_bg, queue_msg_bg_rect)
            canvas.blit(queue_msg_surf, queue_msg_rect)
            
            msg_data[1] -= 1
            if msg_data[1] <= 0:
                self.msg_queue.pop(0)
                
    def handle_item(self, item):
        self.player_vehicle.apply_item(item)
        self.msg_queue.append(self.item_table[item][:])
            
    def process_bullets(self):
        for bullet in self.bullets:
            bullet.update()
            bullet_rect = bullet.get_pos_rect()
            player_hitbox = self.player_vehicle.get_hitbox()
            
            if not bullet.friendly and player_hitbox.colliderect(bullet_rect):
                self.player_vehicle.on_damage(bullet.damage)
                bullet.hit = True
            
            elif bullet.friendly:
                for enemy in self.enemies:
                    enemy_hitbox = enemy.get_hitbox()
                    if bullet.friendly and enemy_hitbox.colliderect(bullet_rect):
                        enemy.on_damage(bullet.damage, self.player_vehicle)
                        bullet.hit = True
                        self.enemy_hit_sound_play = True
                        break # Only one enemy should be hit per bullet
                 
                if bullet.hit:        
                    alert_rect = pygame.Rect(0, 0, 500, 500)
                    alert_rect.center = bullet_rect.center
                    
                    for enemy in self.enemies:
                        enemy_rect = enemy.get_pos_rect()
                        
                        if alert_rect.colliderect(enemy_rect):
                            enemy.aware = True
                            enemy.aware_timeout = 5 * FPS

    def clean_bullets(self):
        bullets_to_remove = []
        
        for bullet in self.bullets:
            if bullet.hit or self.check_immobile_collision(bullet.get_pos_rect()):
                bullets_to_remove.append(bullet)
                
        for dead_bullet in bullets_to_remove:
            self.bullets.remove(dead_bullet)
            
    def clean_enemies(self):
        enemies_to_remove = []
        
        for enemy in self.enemies:
            if enemy.health <= 0:
                enemies_to_remove.append(enemy)
                self.enemy_death_sound_play = True
                
        for dead_enemy in enemies_to_remove:
            self.enemies.remove(dead_enemy)
        
    def check_immobile_collision(self, rect):
        for barrier in self.barriers:
            if rect.colliderect(barrier.get_pos_rect()):
                return True
        return False
        
    def check_site_collision(self, rect):
        for site in self.dig_sites:
            if rect.colliderect(site.get_pos_rect()):
                return True
        return False
    
    def check_enemy_collision(self, rect):
        for enemy in self.enemies:
            if rect.colliderect(enemy.get_pos_rect()):
                return True
        return False
        
    def check_tele_collision(self, rect):
        if self.teleporter.get_pos_rect().colliderect(rect):
            return True
        return False
        
    def check_key_items(self):
        for site in self.dig_sites:
            if site.key_item and not site.drilled:
                return True
        return False
