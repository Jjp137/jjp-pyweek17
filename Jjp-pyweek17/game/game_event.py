import pygame
from pygame.locals import *

from constants import *

class GameEvent(object):
    def __init__(self, event):
        self.original_event = event
        
        self.numbers = {K_1 : EQUIP_ONE,
                        K_2 : EQUIP_TWO,
                        K_3 : EQUIP_THREE,
                        K_4 : EQUIP_FOUR,
                        K_5 : EQUIP_FIVE}
        
        self.button_state = self.get_button_state(event)
        self.action = self.get_action(event)
        
        self.involves_mouse = False
        self.window_pos = None
        
        if event.type in (MOUSEBUTTONDOWN, MOUSEBUTTONUP):
            self.window_pos = event.pos
            self.involves_mouse = True
    
    def get_button_state(self, event):
        if event.type in (KEYDOWN, MOUSEBUTTONDOWN):
            return PRESSED
        elif event.type in (KEYUP, MOUSEBUTTONUP):
            return RELEASED
    
    def get_action(self, event):
        if event.type in (KEYDOWN, KEYUP):
            if event.key == K_w or event.key == K_UP:
                return MOVE_UP
            if event.key == K_s or event.key == K_DOWN:
                return MOVE_DOWN
            if event.key == K_a or event.key == K_LEFT:
                return MOVE_LEFT
            if event.key == K_d or event.key == K_RIGHT:
                return MOVE_RIGHT
            if event.key == K_SPACE:
                return OTHER_ACTION
            if event.key in self.numbers.keys():
                return self.numbers[event.key]
        elif event.type in (MOUSEBUTTONDOWN, MOUSEBUTTONUP):
            if event.button == 1:
                return EQUIP_ACTION
        else:
            return EVENT_IGNORE
        
