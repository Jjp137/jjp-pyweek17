# Starting point

import pygame
import os
import sys
from pygame.locals import *

import pickle
import data
import profile
import screens
from constants import *

def main():
    pygame.init()
    clock = pygame.time.Clock()
    
    canvas = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption("Jjp-pyweek17")
    
    if os.path.exists(data.savepath()):
        user_data = pickle.load(open(data.savepath(), 'rb'))
    else:
        user_data = profile.Profile()
    
    screen_stack = screens.ScreenStack(clock)
    main_menu = screens.MainMenuScreen(screen_stack, canvas, clock, user_data)
    screen_stack.add_screen(main_menu)
    
    while True:
        screen_stack.loop()
        
if __name__ == "__main__":
    main()
