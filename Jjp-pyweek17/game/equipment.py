# All equipment in the game

import math
import pygame
from pygame.locals import *

import entities
import util
from constants import *

class Drill(object):
    def __init__(self, level):
        self.level = level
        self.active = False
        self.progress = 0
        
        self.time = [None, 3.0, 2.5, 2.0, 1.5, 1.0]
        self.name = "Drill"
        
    def read_input(self, player, event):
        if event.action == EQUIP_ACTION:
            if event.button_state == PRESSED:
                player.frozen = True
                self.active = True
            elif event.button_state == RELEASED:
                player.frozen = False
                self.active = False
                self.progress = 0
                
    def store_pointer(self, pointer):
        pass
        
    def update(self, player, terrain):
        if self.active:
            self.progress += 1
        
        if self.progress == int(self.time[self.level] * FPS):
            player_rect = player.get_pos_rect()
            
            for site in terrain.dig_sites:
                if site.get_pos_rect().collidepoint(player_rect.center) \
                and not site.drilled:
                    site.drilled = True
                    terrain.handle_item(site.item)
                    
            self.active = False
            self.progress = 0
            player.frozen = False
            
    def on_inactive_equip(self, player):
        self.active = False
        self.progress = 0
        player.frozen = False
        
    def on_active_equip(self, player):
        pass
        
    def on_no_fuel(self, player):
        self.active = False
        self.progress = 0
        player.frozen = False
    
    def get_status_info(self):
        return (self.name, self.progress, int(self.time[self.level] * FPS))


class Scanner(object):
    def __init__(self, level):
        self.level = level
        self.active = False
        self.progress = 0
        
        self.time = [None, 4.0, 3.5, 3.0, 2.5, 2.0]
        self.reveal_time = [None, 60, 90, 120, 180, 360]
        self.name = "Scanner"
        
    def read_input(self, player, event):
        if event.action == EQUIP_ACTION:
            if event.button_state == PRESSED:
                player.frozen = True
                self.active = True
            elif event.button_state == RELEASED:
                player.frozen = False
                self.active = False
                self.progress = 0
                
    def store_pointer(self, pointer):
        pass

    def update(self, player, terrain):
        if self.active:
            self.progress += 1

        if self.progress == int(self.time[self.level] * FPS):
            scan_rect = pygame.Rect(0, 0, SCANRANGE, SCANRANGE)
            scan_rect.center = player.get_pos_rect().center
            
            for site in terrain.dig_sites:
                if scan_rect.contains(site.get_pos_rect()):
                    site.reveal(FPS * self.reveal_time[self.level])
                    player.scanner_sound_play = True
                    
            self.active = False
            self.progress = 0
            player.frozen = False
            
    def on_inactive_equip(self, player):
        self.active = False
        self.progress = 0
        player.frozen = False
        
    def on_active_equip(self, player):
        pass
        
    def on_no_fuel(self, player):
        self.active = False
        self.progress = 0
        player.frozen = False
            
    def get_status_info(self):
        return (self.name, self.progress, int(self.time[self.level] * FPS))


class MachineGun(object):
    def __init__(self, level):
        self.level = level
        self.active = False
        self.cooldown = 0
        
        self.delay = [None, 0.4, 0.35, 0.3, 0.25, 0.2]
        self.damage = [None, 2, 4, 6, 8, 10]
        self.name = "Machine Gun"
        self.mouse_pos = None
        
    def read_input(self, player, event):
        if event.action == EQUIP_ACTION:
            if event.button_state == PRESSED:
                self.active = True
            elif event.button_state == RELEASED:
                self.active = False
                
    def store_pointer(self, pointer):
        self.mouse_pos = pointer
    
    def update(self, player, terrain):
        if self.cooldown > 0:
            self.cooldown -= 1
        
        if self.cooldown == 0 and self.active:
            self.shoot_bullet(player, terrain)
            self.cooldown = int(self.delay[self.level] * FPS)
            player.shoot_sound_play = True
    
    def shoot_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        
        bullet_vel = util.velocity_vector(player_pos, self.mouse_pos, 6)
        
        damage = self.damage[self.level]
        new_bullet = entities.Bullet(player_pos, bullet_vel, damage, BLUE, True)
        terrain.bullets.append(new_bullet)
    
    def on_inactive_equip(self, player):
        self.active = False
        
    def on_active_equip(self, player):
        if pygame.mouse.get_pressed()[0] == True:
            self.active = True
    
    def on_no_fuel(self, player):
        self.active = False
        self.cooldown = 0
        
    def get_status_info(self):
        return (self.name, self.cooldown, int(self.delay[self.level] * FPS))


class SpreadGun(MachineGun):
    def __init__(self, level):
        self.level = level
        self.active = False
        self.cooldown = 0
        
        self.delay = [None, 0.5, 0.4, 0.4, 0.3, 0.3]
        self.damage = [None, 2, 4, 4, 6, 6]
        self.spread_count = [None, 3, 3, 5, 5, 7]
        self.name = "Spread Gun"
        self.mouse_pos = None
    
    def shoot_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        
        center_angle = util.get_angle(player_pos, self.mouse_pos)
        angle_start = center_angle - (math.pi / 16) * (self.spread_count[self.level] / 2)
        damage = self.damage[self.level]
        
        for i in range(self.spread_count[self.level]):
            bullet_angle = angle_start + (math.pi / 16) * i
            bullet_vel = util.velocity_with_angle(bullet_angle, 5)
            new_bullet = entities.Bullet(player_pos, bullet_vel, damage, BLUE, True, 5)
            
            terrain.bullets.append(new_bullet)

class Missiles(MachineGun):
    def __init__(self, level, count):
        self.level = level
        self.active = False
        self.cooldown = 0
        
        self.delay = [None, 1.0, 0.8, 0.6, 0.5, 0.4]
        self.damage = [None, 30, 40, 60, 80, 100]
        self.max_count = [None, 10, 15, 20, 25, 30]
        
        self.name = "Missiles"
        self.mouse_pos = None
        self.count = count
    
    def update(self, player, terrain):
        if self.cooldown > 0:
            self.cooldown -= 1
        
        if self.cooldown == 0 and self.active and self.count > 0:
            self.shoot_bullet(player, terrain)
            self.cooldown = int(self.delay[self.level] * FPS)
            player.missile_sound_play = True
    
    def shoot_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        
        bullet_vel = util.velocity_vector(player_pos, self.mouse_pos, 8)
        
        damage = self.damage[self.level]
        new_bullet = entities.Bullet(player_pos, bullet_vel, damage, RED, True, 12)
        terrain.bullets.append(new_bullet)
        
        self.count -= 1
        
    def get_status_info(self):
        first_info = self.name + " (Left: " + str(self.count) + "/" + str(self.max_count[self.level]) + ")"
        return (first_info, self.cooldown, int(self.delay[self.level] * FPS))
