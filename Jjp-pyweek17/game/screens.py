import pygame
import sys
from pygame.locals import *

import buttons
import game_event
import moon_map
import data
import terrain
import camera
import statusbar
import shop
import util
from constants import *

class ScreenStack(object):
    def __init__(self, clock):
        self.stack = []
        self.clock = clock
    
    def add_screen(self, new_screen, hide_others=True):
        if hide_others:
            for screen in self.stack:
                screen.visible = False
        
        self.stack.append(new_screen)
        
    def pop_screen(self):
        del self.stack[-1]
        
        self.stack[-1].visible = True
        
    def clear_stack(self):
        self.stack = []
        
    def loop(self):
        self.stack[-1].read_input()
        self.stack[-1].update()
        
        for screen in self.stack:
            if screen.visible:
                screen.draw()
        
        pygame.display.update()
        self.clock.tick(FPS)


class MainMenuScreen(object):
    def __init__(self, stack, canvas, clock, profile):
        self.visible = True
        self.screen_stack = stack
        
        self.canvas = canvas
        self.clock = clock
        self.profile = profile

        self.buttons = []
        self.moon_pic = util.load_alpha_image("the-moon.png")
        self.small_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        self.big_font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 64)
        
        self.create_buttons()
        
    def create_buttons(self):
        start_game_pos = (200, 300)
        start_game_size = (400, 50)
        start_game_button = buttons.Button(start_game_pos, start_game_size, 
            "Start Game", 32, self.begin_game)
        
        options_pos = (200, 370)
        options_size = (400, 50)
        options_button = buttons.Button(options_pos, options_size, 
            "Options", 32, self.open_options)
        
        exit_game_pos = (200, 440)
        exit_game_size = (400, 50)
        exit_game_button = buttons.Button(exit_game_pos, exit_game_size, 
            "Quit Game", 32, self.end_game)
            
        self.buttons.append(start_game_button)
        self.buttons.append(options_button)
        self.buttons.append(exit_game_button)

    def read_input(self):
        mouse_pointer = pygame.mouse.get_pos()
        for button in self.buttons:
            button.read_pointer(mouse_pointer)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
                
            for button in self.buttons:
                button.read_input(event)
                    
    def update(self):
        pass
    
    def draw(self):
        self.canvas.fill(BLACK)
        
        moon_pos = (144, 69)
        self.canvas.blit(self.moon_pic, moon_pos)
        
        # The "options" text
        title_bg_rect = pygame.Rect(10, 50, 780, 100)
        pygame.draw.rect(self.canvas, BLACK, title_bg_rect)
        pygame.draw.rect(self.canvas, WHITE, title_bg_rect, 5)
        
        title_surf = self.big_font.render("The Lunar Project", True, WHITE)
        title_rect = title_surf.get_rect()
        title_rect.center = title_bg_rect.center
        self.canvas.blit(title_surf, title_rect)
        
        for button in self.buttons:
            button.draw(self.canvas)
        
        help_string = "Click on a menu option to select it."
        msg_surf = self.small_font.render(help_string, True, WHITE)
        msg_rect = msg_surf.get_rect()
        msg_rect.topleft = (10, 620)
        self.canvas.blit(msg_surf, msg_rect)

    def begin_game(self):
        new_screen = MapScreen(self.screen_stack, self.canvas, self.clock, 
            self.profile)
        self.screen_stack.add_screen(new_screen)
        
    def open_options(self):
        new_screen = OptionsScreen(self.screen_stack, self.canvas, self.clock, 
            self.profile)
        self.screen_stack.add_screen(new_screen)
        
    def end_game(self):
        self.profile.save()
        
        pygame.quit()
        sys.exit()


class OptionsScreen(object):
    def __init__(self, stack, canvas, clock, profile):
        self.visible = True
        self.screen_stack = stack
        
        self.canvas = canvas
        self.clock = clock
        self.profile = profile
        
        self.buttons = []
        self.moon_pic = util.load_alpha_image("the-moon.png")
        self.small_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        self.big_font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 64)
        
        self.clear_flag = False
        self.clear_msg_timeout = 0
        
        self.create_buttons()
        
    def create_buttons(self):
        music_toggle_pos = (200, 300)
        music_toggle_size = (400, 50)
        music_text = "Music: On" if self.profile.music else "Music: Off"
        music_toggle_button = buttons.Button(music_toggle_pos, music_toggle_size, 
            music_text, 32, self.toggle_music)
        music_toggle_button.selectable = False
        
        sound_toggle_pos = (200, 370)
        sound_toggle_size = (400, 50)
        sound_text = "Sound: On" if self.profile.sound else "Sound: Off"
        sound_toggle_button = buttons.Button(sound_toggle_pos, sound_toggle_size, 
            sound_text, 32, self.toggle_sound)
        
        back_pos = (200, 440)
        back_size = (400, 50)
        back_button = buttons.Button(back_pos, back_size, 
            "Back to Main Menu", 32, self.exit_options)
            
        clear_pos = (700, 620)
        clear_size = (80, 20)
        clear_text = "Yes, really!" if self.clear_flag else "Clear Data"
        clear_button = buttons.Button(clear_pos, clear_size, 
            clear_text, 10, self.clear_data)
            
        self.buttons.append(music_toggle_button)
        self.buttons.append(sound_toggle_button)
        self.buttons.append(back_button)
        self.buttons.append(clear_button)
        
    def read_input(self):
        mouse_pointer = pygame.mouse.get_pos()
        for button in self.buttons:
            button.read_pointer(mouse_pointer)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            for button in self.buttons:
                button.read_input(event)
    
    def update(self):
        pass
    
    def draw(self):
        self.canvas.fill(BLACK)
        
        moon_pos = (144, 69)
        self.canvas.blit(self.moon_pic, moon_pos)
        
        # The "options" text
        title_bg_rect = pygame.Rect(50, 50, 700, 100)
        pygame.draw.rect(self.canvas, BLACK, title_bg_rect)
        pygame.draw.rect(self.canvas, WHITE, title_bg_rect, 5)
        
        title_surf = self.big_font.render("Options", True, WHITE)
        title_rect = title_surf.get_rect()
        title_rect.center = title_bg_rect.center
        self.canvas.blit(title_surf, title_rect)
        
        for button in self.buttons:
            button.draw(self.canvas)
        
        help_string = None
        if self.clear_flag:
            help_string = "Are you sure?"
        elif self.clear_msg_timeout > 0:
            help_string = "Data cleared."
            self.clear_msg_timeout -= 1
            
        if help_string != None:
            msg_surf = self.small_font.render(help_string, True, WHITE)
            msg_rect = msg_surf.get_rect()
            msg_rect.topleft = (550, 620)
            self.canvas.blit(msg_surf, msg_rect)

    def toggle_sound(self):
        self.profile.sound = not self.profile.sound
        self.refresh_buttons()
        
    def toggle_music(self):
        self.profile.music = not self.profile.music
        self.refresh_buttons()
        
    def refresh_buttons(self):
        self.buttons = []
        self.create_buttons()
        
    def exit_options(self):
        self.screen_stack.pop_screen()
        
    def clear_data(self):
        if not self.clear_flag:
            self.clear_flag = True
            self.clear_msg_timeout = 0
        elif self.clear_flag:
            self.clear_flag = False
            self.clear_msg_timeout = 3 * FPS
            self.profile.new_profile()
            self.profile.save()
            
        self.refresh_buttons()


class GameplayScreen(object):
    def __init__(self, stack, canvas, clock, profile, filename):
        self.visible = True
        self.screen_stack = stack
        
        self.canvas = canvas
        self.clock = clock
        self.profile = profile
        
        self.world = terrain.Terrain(data.load(filename, 'r'), self.profile)
        self.cam = camera.Camera(self.world)
        self.status = statusbar.StatusBar(self.world)
        
    def read_input(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
                
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                pause_overlay = PauseScreen(self.screen_stack, self.canvas, 
                    self.clock, self.profile)
                self.screen_stack.add_screen(pause_overlay, False)
                return # Don't process further input
            
            action = game_event.GameEvent(event)
            self.world.read_input(action)
            
            if self.world.screen_flag != None:
                self.return_to_map()
                return # Don't process further input
            
        mouse_draw_pos = pygame.mouse.get_pos()
        mouse_world_pos = self.cam.get_world_pos(mouse_draw_pos)
        self.world.read_pointer(mouse_world_pos)
        
    def update(self):
        self.world.update()
        self.cam.update_pos()
        self.status.update(self.clock.get_fps())
        
    def draw(self):
        if self.profile.sound:
            self.world.play_sound()
        
        self.canvas.fill(BLACK)
        self.cam.draw_field(self.canvas)
        self.world.draw_messages(self.canvas)
        self.status.draw(self.canvas)

    def return_to_map(self):
        self.screen_stack.stack[-2].rebuild_signal = True # Ugly hack
        self.screen_stack.pop_screen()

class MapScreen(object):
    def __init__(self, stack, canvas, clock, profile):
        self.visible = True
        self.screen_stack = stack
        
        self.canvas = canvas
        self.clock = clock
        self.profile = profile
        
        self.buttons = []
        self.moon_pic = util.load_alpha_image("the-moon.png")
        self.small_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        self.middle_font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 32)
        self.big_font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 64)
        
        self.dest_map = moon_map.MoonMap(self.profile)
        
        self.save_msg_timeout = 0
        self.rebuild_signal = False
        
        self.create_buttons()
        
    def create_buttons(self):
        land_pos = (450, 475)
        land_size = (330, 50)
        land_text = "Travel"
        land_button = buttons.Button(land_pos, land_size, land_text,
            32, self.start_map)
        land_button.selectable = False
        
        shop_pos = (20, 550)
        shop_size = (200, 50)
        shop_text = "Shop"
        shop_button = buttons.Button(shop_pos, shop_size, shop_text,
            32, self.open_shop_menu)
            
        save_pos = (300, 550)
        save_size = (200, 50)
        save_text = "Save"
        save_button = buttons.Button(save_pos, save_size, save_text,
            32, self.save_profile)
            
        back_pos = (580, 550)
        back_size = (200, 50)
        back_text = "Back"
        back_button = buttons.Button(back_pos, back_size, back_text,
            32, self.go_back)

        self.buttons.append(land_button)
        self.buttons.append(shop_button)
        self.buttons.append(save_button)
        self.buttons.append(back_button)
        
    def read_input(self):
        mouse_pointer = pygame.mouse.get_pos()
        for button in self.buttons:
            button.read_pointer(mouse_pointer)
            
        self.dest_map.read_pointer(mouse_pointer)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
                
            for button in self.buttons:
                button.read_input(event)
                
            self.dest_map.read_input(event)
        
    def update(self):
        if self.dest_map.selected_dest != None:
            self.buttons[0].selectable = True
            
        elif self.rebuild_signal:
            self.dest_map = moon_map.MoonMap(self.profile)
            self.rebuild_signal = False
    
    def draw(self):
        self.canvas.fill(BLACK)
        
        moon_pos = (144, 69)
        self.canvas.blit(self.moon_pic, moon_pos)
        
        title_bg_rect = pygame.Rect(50, 50, 700, 100)
        pygame.draw.rect(self.canvas, BLACK, title_bg_rect)
        pygame.draw.rect(self.canvas, WHITE, title_bg_rect, 5)
        
        title_surf = self.big_font.render("Excavation Map", True, WHITE)
        title_rect = title_surf.get_rect()
        title_rect.center = title_bg_rect.center
        self.canvas.blit(title_surf, title_rect)
        
        dest_bg_rect = pygame.Rect(20, 475, 410, 50)
        pygame.draw.rect(self.canvas, BLACK, dest_bg_rect)
        pygame.draw.rect(self.canvas, WHITE, dest_bg_rect, 5)
        
        dest_surf = self.middle_font.render(self.dest_map.dest_name(), True, WHITE)
        dest_rect = dest_surf.get_rect()
        dest_rect.topleft = (25, 480)
        self.canvas.blit(dest_surf, dest_rect)
        
        for button in self.buttons:
            button.draw(self.canvas)
            
        self.dest_map.draw(self.canvas)
            
        help_string = None
        if self.save_msg_timeout > 0:
            help_string = "Game saved."
        elif self.profile.key_items[RED_CRYSTAL]:
            help_string = "Game complete! Thank you for playing!"
            
        if help_string != None:
            msg_surf = self.small_font.render(help_string, True, WHITE)
            msg_rect = msg_surf.get_rect()
            msg_rect.topleft = (20, 620)
            self.canvas.blit(msg_surf, msg_rect)
            
            self.save_msg_timeout -= 1

    def start_map(self):
        dest_to_use = self.dest_map.selected_dest
        
        new_screen = GameplayScreen(self.screen_stack, self.canvas, self.clock, 
            self.profile, dest_to_use.filename)
        self.screen_stack.add_screen(new_screen)
        
        self.dest_map.selected_dest = None
        self.buttons[0].selectable = False
    
    def open_shop_menu(self):
        new_screen = ShopScreen(self.screen_stack, self.canvas, self.clock, 
            self.profile)
        self.screen_stack.add_screen(new_screen)
        
    def save_profile(self):
        self.profile.save()
        self.save_msg_timeout = 3 * FPS
    
    def go_back(self):
        self.screen_stack.pop_screen()


class ShopScreen(object):
    def __init__(self, stack, canvas, clock, profile):
        self.visible = True
        self.screen_stack = stack
        
        self.canvas = canvas
        self.clock = clock
        self.profile = profile
        
        self.buttons = []
        self.moon_pic = util.load_alpha_image("the-moon.png")
        self.small_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        self.big_font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 64)
        
        self.shop = shop.Shop(self.profile)
        
        self.create_buttons()
        
    def create_buttons(self):
        back_pos = (460, 550)
        back_size = (300, 50)
        back_text = "Back"
        back_button = buttons.Button(back_pos, back_size, back_text,
            32, self.go_back)
            
        self.buttons.append(back_button)
        
    def read_input(self):
        mouse_pointer = pygame.mouse.get_pos()
        self.shop.read_pointer(mouse_pointer)
        for button in self.buttons:
            button.read_pointer(mouse_pointer)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            for button in self.buttons:
                button.read_input(event)
                
            self.shop.read_input(event)
        
    def update(self):
        pass
    
    def draw(self):
        self.canvas.fill(BLACK)

        moon_pos = (144, 69)
        self.canvas.blit(self.moon_pic, moon_pos)
        
        title_bg_rect = pygame.Rect(50, 50, 700, 100)
        pygame.draw.rect(self.canvas, BLACK, title_bg_rect)
        pygame.draw.rect(self.canvas, WHITE, title_bg_rect, 5)
        
        title_surf = self.big_font.render("Shop", True, WHITE)
        title_rect = title_surf.get_rect()
        title_rect.center = title_bg_rect.center
        self.canvas.blit(title_surf, title_rect)
        
        for button in self.buttons:
            button.draw(self.canvas)
            
        self.shop.draw(self.canvas)
        
    def go_back(self):
        self.screen_stack.pop_screen()


class PauseScreen(object):
    def __init__(self, stack, canvas, clock, profile):
        self.visible = True
        self.screen_stack = stack
        
        self.canvas = canvas
        self.clock = clock
        self.profile = profile
        
        self.buttons = []
        self.big_font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 64)
        
        self.create_buttons()
        
    def create_buttons(self):
        resume_pos = (200, 200)
        resume_size = (400, 50)
        resume_text = "Resume Game"
        resume_button = buttons.Button(resume_pos, resume_size, resume_text,
            32, self.resume_game)
        
        music_toggle_pos = (200, 270)
        music_toggle_size = (400, 50)
        music_text = "Music: On" if self.profile.music else "Music: Off"
        music_toggle_button = buttons.Button(music_toggle_pos, music_toggle_size, 
            music_text, 32, self.toggle_music)
        music_toggle_button.selectable = False
        
        sound_toggle_pos = (200, 340)
        sound_toggle_size = (400, 50)
        sound_text = "Sound: On" if self.profile.sound else "Sound: Off"
        sound_toggle_button = buttons.Button(sound_toggle_pos, sound_toggle_size, 
            sound_text, 32, self.toggle_sound)
        
        map_pos = (200, 410)
        map_size = (400, 50)
        map_button = buttons.Button(map_pos, map_size, 
            "Back to Map", 32, self.back_to_map)
            
        self.buttons.append(resume_button)
        self.buttons.append(music_toggle_button)
        self.buttons.append(sound_toggle_button)
        self.buttons.append(map_button)
        
    def read_input(self):
        mouse_pointer = pygame.mouse.get_pos()
        for button in self.buttons:
            button.read_pointer(mouse_pointer)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
                
            if event.type == KEYDOWN and event.key == K_ESCAPE:
                self.resume_game()
                return
                
            for button in self.buttons:
                button.read_input(event)
        
    def update(self):
        pass
    
    def draw(self):
        bg_size = (WINDOW_WIDTH, WINDOW_HEIGHT)
        
        bg_surface = pygame.Surface(bg_size).convert_alpha()
        bg_surface.fill(TRANS_BLACK)
        bg_rect = bg_surface.get_rect()
        bg_rect.topleft = (0, 0)
        
        self.canvas.blit(bg_surface, bg_rect)
        
        title_bg_rect = pygame.Rect(50, 50, 700, 100)
        pygame.draw.rect(self.canvas, BLACK, title_bg_rect)
        pygame.draw.rect(self.canvas, WHITE, title_bg_rect, 5)
        
        title_surf = self.big_font.render("Pause Menu", True, WHITE)
        title_rect = title_surf.get_rect()
        title_rect.center = title_bg_rect.center
        self.canvas.blit(title_surf, title_rect)
        
        for button in self.buttons:
            button.draw(self.canvas)
        
    def resume_game(self):
        self.screen_stack.pop_screen()
        
    def toggle_sound(self):
        self.profile.sound = not self.profile.sound
        self.refresh_buttons()
        
    def toggle_music(self):
        self.profile.music = not self.profile.music
        self.refresh_buttons()
        
    def back_to_map(self):
        self.screen_stack.pop_screen()
        self.screen_stack.pop_screen()
        
    def refresh_buttons(self):
        self.buttons = []
        self.create_buttons()
