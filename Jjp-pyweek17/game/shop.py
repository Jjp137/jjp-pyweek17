import pygame
from pygame.locals import *

import buttons
import data
from constants import *

class Shop(object):
    def __init__(self, profile):
        self.profile = profile
        self.font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), 16)
        self.buy_sound = pygame.mixer.Sound(data.filepath("powerup.wav"))
        
        self.build_buttons()
        
    def build_buttons(self):
        self.buttons = []
        self.selected_button = None
        self.buy_action = None
        self.item_label = None
        self.current_label = None
        self.next_label = None
        self.buy_cost = None
        
        upgrade_button_size = (185, 40)
        
        health_up_pos = (50, 200)
        health_up_text = "Health"
        health_up_button = buttons.ShopButton(health_up_pos, upgrade_button_size, 
            health_up_text, 16, self.check_health_upgrade)
        self.buttons.append(health_up_button)
            
        fuel_up_pos = (265, 200)
        fuel_up_text = "Fuel"
        fuel_up_button = buttons.ShopButton(fuel_up_pos, upgrade_button_size, 
            fuel_up_text, 16, self.check_fuel_upgrade)
        self.buttons.append(fuel_up_button)
            
        speed_up_pos = (50, 250)
        speed_up_text = "Speed"
        speed_up_button = buttons.ShopButton(speed_up_pos, upgrade_button_size,
            speed_up_text, 16, self.check_speed)
        self.buttons.append(speed_up_button)
            
        drill_pos = (265, 250)
        drill_text = "Drill"
        drill_button = buttons.ShopButton(drill_pos, upgrade_button_size,
            drill_text, 16, self.check_drill)
        self.buttons.append(drill_button)
            
        scanner_pos = (50, 300)
        scanner_text = "Scanner"
        scanner_button = buttons.ShopButton(scanner_pos, upgrade_button_size,
            scanner_text, 16, self.check_scanner)
        self.buttons.append(scanner_button)
        
        if self.profile.key_items[GUN_SCHEME]:
            gun_pos = (265, 300)
            gun_text = "Machine Gun" if self.profile.machine_gun_level >= 1 else "Buy Machine Gun"
            gun_button = buttons.ShopButton(gun_pos, upgrade_button_size, gun_text,
                16, self.check_machine_gun)
            self.buttons.append(gun_button)
        
        if self.profile.key_items[SPREAD_SCHEME]:    
            spread_pos = (50, 350)
            spread_text = "Spread Gun" if self.profile.spread_level >= 1 else "Buy Spread Gun"
            spread_button = buttons.ShopButton(spread_pos, upgrade_button_size, spread_text,
                16, self.check_spread)
            self.buttons.append(spread_button)
        
        if self.profile.key_items[MISSILE_SCHEME]:
            missile_pos = (265, 350)
            missile_text = "Missiles" if self.profile.missile_level >= 1 else "Buy Missiles"
            missile_button = buttons.ShopButton(missile_pos, upgrade_button_size, missile_text,
                16, self.check_missiles)
            self.buttons.append(missile_button)
            
        health_refill_pos = (50, 440)
        health_refill_text = "Health"
        health_refill_button = buttons.ShopButton(health_refill_pos, upgrade_button_size,
            health_refill_text, 16, self.check_health_refill)
        self.buttons.append(health_refill_button)
            
        fuel_refill_pos = (265, 440)
        fuel_refill_text = "Fuel"
        fuel_refill_button = buttons.ShopButton(fuel_refill_pos, upgrade_button_size,
            fuel_refill_text, 16, self.check_fuel_refill)
        self.buttons.append(fuel_refill_button)
        
        if self.profile.missile_level >= 1:
            missile_refill_pos = (50, 490)
            missile_refill_text = "Missiles"
            missile_refill_button = buttons.ShopButton(missile_refill_pos, upgrade_button_size,
                missile_refill_text, 16, self.check_missile_refill)
            self.buttons.append(missile_refill_button)
            
        buy_pos = (460, 360)
        buy_size = (290, 50)
        buy_text = "Buy!"
        buy_button = buttons.ShopButton(buy_pos, buy_size, buy_text, 32, self.buy_callback)
        buy_button.selectable = False
        self.buttons.append(buy_button)
        
    def read_pointer(self, pointer):
        for button in self.buttons:
            button.read_pointer(pointer)
        
    def read_input(self, event):
        for button in self.buttons:
            was_selected = button.selected
            
            button.read_input(event)
            if not was_selected and button.selected:
                self.selected_button = button
        
        for button in self.buttons:
            if button != self.selected_button:
                button.selected = False
        
    def draw(self, canvas):
        self.draw_textbox(canvas, pygame.Rect(50, 160, 400, 30), "Upgrades")
        self.draw_textbox(canvas, pygame.Rect(50, 400, 400, 30), "Refills")
        
        self.draw_description(canvas)
        
        for button in self.buttons:
            button.draw(canvas)
            
    def draw_textbox(self, canvas, rect, string):
        pygame.draw.rect(canvas, BLACK, rect)
        pygame.draw.rect(canvas, WHITE, rect, 5)

        msg_surf = self.font.render(string, True, WHITE)
        msg_rect = msg_surf.get_rect()
        msg_rect.center = rect.center
        canvas.blit(msg_surf, msg_rect)
    
    def draw_text(self, canvas, topleft, string):
        msg_surf = self.font.render(string, True, WHITE)
        msg_rect = msg_surf.get_rect()
        msg_rect.topleft = topleft
        canvas.blit(msg_surf, msg_rect)
        
    def draw_description(self, canvas):
        description_rect = pygame.Rect(460, 160, 290, 150)
        pygame.draw.rect(canvas, BLACK, description_rect)
        pygame.draw.rect(canvas, WHITE, description_rect, 5)
        
        money_rect = pygame.Rect(460, 320, 290, 30)
        money_string = "Money: " + str(self.profile.money)
        pygame.draw.rect(canvas, BLACK, money_rect)
        pygame.draw.rect(canvas, WHITE, money_rect, 5)
        self.draw_text(canvas, (465, 325), money_string)
        
        if self.item_label == None:
            first_line = "Select an item."
        else:
            first_line = self.item_label
        
        self.draw_text(canvas, (465, 165), first_line)
        
        if self.current_label != None:    
            second_line = self.current_label
            self.draw_text(canvas, (465, 225), second_line)
        if self.next_label != None:
            third_line = self.next_label
            self.draw_text(canvas, (465, 250), third_line)
            
        fourth_line = ""
        if self.buy_cost != None:
            fourth_line = "Price: " + str(self.buy_cost)
        elif self.item_label != None:
            fourth_line = "Price: n/a"
        
        self.draw_text(canvas, (465, 275), fourth_line)
        
    def update_description(self, action, labels, cost):
        self.buy_action = action
        if self.buy_action != None and cost <= self.profile.money:
            self.buttons[-1].selectable = True
        else:
            self.buttons[-1].selectable = False
        
        self.item_label = labels[0]
        self.current_label = labels[1]
        self.next_label = labels[2]
        self.buy_cost = cost
        
    def buy_callback(self):
        if self.profile.sound:
            self.buy_sound.play()
            
        self.buy_action()
        
    def make_label_tuple(self, first_line, second_line, third_line):
        return (first_line, second_line, third_line)
    
    def check_speed(self):
        cost_table = [None, 300, 900, 2700, 5400, None]
        current_level = self.profile.speed_level
        next_level = current_level + 1 if current_level < 5 else "n/a"
        cost = cost_table[current_level]
        
        first_line = "Increase movement speed."
        second_line = "Current level: " + str(current_level)
        third_line = "Next level: " + str(next_level)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_speed, display_tuple, cost)

    def check_health_upgrade(self):
        cost_table = {30 : 100, 40 : 200, 50 : 400, 60 : 800, 70: 1600,
            80 : 3200, 90 : 3200, 100 : None}
        max_health = self.profile.max_health
        next_max_health = max_health + 10 if max_health < 100 else "n/a"
        cost = cost_table[max_health]
        
        first_line = "Increase max health by 10."
        second_line = "Current max health: " + str(max_health)
        third_line = "Next max health: " + str(next_max_health)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_health, display_tuple, cost)
    
    def check_fuel_upgrade(self):
        cost_table = {50 : 100, 60 : 200, 70 : 400, 80 : 800, 90: 1600,
            100 : 3200, 110 : 3200, 120 : None}
        max_fuel = self.profile.max_fuel
        next_max_fuel = max_fuel + 10 if max_fuel < 120 else "n/a"
        cost = cost_table[max_fuel]
        
        first_line = "Increase max fuel by 10."
        second_line = "Current max fuel: " + str(max_fuel)
        third_line = "Next max fuel: " + str(next_max_fuel)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_fuel, display_tuple, cost)
        
    def check_drill(self):
        cost_table = [None, 50, 150, 300, 900, None]
        current_level = self.profile.drill_level
        next_level = current_level + 1 if current_level < 5 else "n/a"
        cost = cost_table[current_level]
        
        first_line = "Upgrade the drill."
        second_line = "Current level: " + str(current_level)
        third_line = "Next level: " + str(next_level)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_drill, display_tuple, cost)
        
    def check_scanner(self):
        cost_table = [None, 50, 150, 300, 900, None]
        current_level = self.profile.scanner_level
        next_level = current_level + 1 if current_level < 5 else "n/a"
        cost = cost_table[current_level]
        
        first_line = "Upgrade the scanner."
        second_line = "Current level: " + str(current_level)
        third_line = "Next level: " + str(next_level)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_scanner, display_tuple, cost)
    
    def check_machine_gun(self):
        cost_table = [100, 500, 1000, 3000, 6000, None]
        current_level = self.profile.machine_gun_level
        next_level = current_level + 1 if current_level < 5 else "n/a"
        cost = cost_table[current_level]
        
        first_line = "Upgrade the machine gun." if current_level > 0 else "Buy the machine gun."
        second_line = "Current level: " + str(current_level)
        third_line = "Next level: " + str(next_level)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_machine_gun, display_tuple, cost)
    
    def check_spread(self):
        cost_table = [250, 500, 1000, 3000, 6000, None]
        current_level = self.profile.spread_level
        next_level = current_level + 1 if current_level < 5 else "n/a"
        cost = cost_table[current_level]
        
        first_line = "Upgrade the spread gun." if current_level > 0 else "Buy the spread gun."
        second_line = "Current level: " + str(current_level)
        third_line = "Next level: " + str(next_level)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_spread, display_tuple, cost)
        
    def check_missiles(self):
        cost_table = [250, 500, 1000, 3000, 6000, None]
        current_level = self.profile.missile_level
        next_level = current_level + 1 if current_level < 5 else "n/a"
        cost = cost_table[current_level]
        
        first_line = "Upgrade the missiles." if current_level > 0 else "Buy the missile launcher."
        second_line = "Current level: " + str(current_level)
        third_line = "Next level: " + str(next_level)
        display_tuple = self.make_label_tuple(first_line, second_line, third_line)
        
        if cost == None:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.upgrade_missiles, display_tuple, cost)
        
    def check_health_refill(self):
        lost_health = self.profile.max_health - self.profile.current_health
        cost = lost_health * 3
        
        first_line = "Restores health."
        third_line = "Current health: " + str(self.profile.current_health) \
            + "/" + str(self.profile.max_health)
        display_tuple = self.make_label_tuple(first_line, "", third_line)
        
        if cost == 0:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.refill_health, display_tuple, cost)
        
    def check_fuel_refill(self):
        lost_fuel = self.profile.max_fuel - self.profile.current_fuel
        cost = lost_fuel * 2
        
        first_line = "Restores fuel."
        third_line = "Current fuel: " + str(self.profile.current_fuel) \
            + "/" + str(self.profile.max_fuel)
        display_tuple = self.make_label_tuple(first_line, "", third_line)
        
        if cost == 0:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.refill_fuel, display_tuple, cost)
        
    def check_missile_refill(self):
        max_count = [None, 10, 15, 20, 25, 30]
        lost_missiles = max_count[self.profile.missile_level] - self.profile.missile_count
        cost = lost_missiles * 5 if lost_missiles > 0 else 0
        
        first_line = "Restores missiles."
        third_line = "Current missiles: " + str(self.profile.missile_count) \
            + "/" + str(max_count[self.profile.missile_level])
        display_tuple = self.make_label_tuple(first_line, "", third_line)
        
        if cost == 0:
            self.update_description(None, display_tuple, None)
        else:
            self.update_description(self.refill_missiles, display_tuple, cost)
    
    def upgrade_speed(self):
        self.profile.money -= self.buy_cost
        self.profile.speed_level += 1
        self.profile.speed += 0.5
        
        self.build_buttons()
        
    def upgrade_health(self):
        self.profile.money -= self.buy_cost
        self.profile.max_health += 10
        self.profile.current_health += 10
        
        self.build_buttons()
    
    def upgrade_fuel(self):
        self.profile.money -= self.buy_cost
        self.profile.max_fuel += 10
        self.profile.current_fuel += 10
        
        self.build_buttons()
        
    def upgrade_drill(self):
        self.profile.money -= self.buy_cost
        self.profile.drill_level += 1
        
        self.build_buttons()
        
    def upgrade_scanner(self):
        self.profile.money -= self.buy_cost
        self.profile.scanner_level += 1
        
        self.build_buttons()
    
    def upgrade_machine_gun(self):
        self.profile.money -= self.buy_cost
        self.profile.machine_gun_level += 1
        
        self.build_buttons()
    
    def upgrade_spread(self):
        self.profile.money -= self.buy_cost
        self.profile.spread_level += 1
        
        self.build_buttons()
        
    def upgrade_missiles(self):
        self.profile.money -= self.buy_cost
        self.profile.missile_level += 1
        
        if self.profile.missile_level == 1:
            self.profile.missile_count = 10
        else:
            self.profile.missile_count += 5
        
        self.build_buttons()
        
    def refill_health(self):
        self.profile.money -= self.buy_cost
        self.profile.current_health = self.profile.max_health
        
        self.build_buttons()
        
    def refill_fuel(self):
        self.profile.money -= self.buy_cost
        self.profile.current_fuel = self.profile.max_fuel
        
        self.build_buttons()
        
    def refill_missiles(self):
        max_count = [None, 10, 15, 20, 25, 30]
        
        self.profile.money -= self.buy_cost
        self.profile.missile_count = max_count[self.profile.missile_level]
        
        self.build_buttons()
        
    
