import pygame
import sys
from pygame.locals import *

import screens
import util
import data
from constants import *

class Button(object):
    def __init__(self, pos, size, text, font_size, callback):
        self.pos = pos
        self.size = size
        self.text = text
        self.font_size = font_size
        self.callback = callback
        self.selectable = True
        
        self.hover = False
        self.clicked = False
        
        self.font = pygame.font.Font(data.filepath('xolonium-bold.ttf'), self.font_size)
        
    def read_pointer(self, pointer):
        if self.get_rect_pos().collidepoint(pointer):
            self.hover = True
        else:
            self.hover = False
        
    def read_input(self, event):
        if not self.selectable:
            return
        
        if event.type == MOUSEBUTTONDOWN and event.button == 1 and self.hover:
            self.clicked = True
        if event.type == MOUSEBUTTONUP and event.button == 1 \
            and self.hover and self.clicked:
            self.callback()
            self.clicked = False
        
    def draw(self, canvas):
        button_rect = self.get_rect_pos()
        pygame.draw.rect(canvas, BLACK, button_rect)
        
        if not self.selectable:
            outline_color = GRAY
        elif self.hover:
            outline_color = YELLOW
        else:
            outline_color = WHITE
            
        pygame.draw.rect(canvas, outline_color, button_rect, 5)
        self.draw_centered_words(canvas)
        
    def get_rect_pos(self):
        return pygame.Rect(self.pos[X_INDEX], self.pos[Y_INDEX], 
            self.size[X_INDEX], self.size[Y_INDEX])
            
    def draw_centered_words(self, canvas):
        if self.selectable and self.hover:
            text_color = YELLOW
        elif self.selectable:
            text_color = WHITE
        else:
            text_color = GRAY
        
        msg_surf = self.font.render(self.text, True, text_color)
        msg_rect = msg_surf.get_rect()
        msg_rect.center = self.get_rect_pos().center
        canvas.blit(msg_surf, msg_rect)

class ShopButton(Button):
    def __init__(self, pos, size, text, font_size, callback):
        super(ShopButton, self).__init__(pos, size, text, font_size, callback)
        
        self.selected = False
        
    def read_input(self, event):
        if not self.selectable:
            return
        
        if event.type == MOUSEBUTTONDOWN and event.button == 1 and self.hover:
            self.clicked = True
        if event.type == MOUSEBUTTONUP and event.button == 1 \
            and self.hover and self.clicked and not self.selected:
            self.callback()
            self.clicked = False
            self.selected = True
        
    def draw(self, canvas):
        button_rect = self.get_rect_pos()
        pygame.draw.rect(canvas, BLACK, button_rect)
        
        if not self.selectable:
            outline_color = GRAY
        elif self.hover or self.selected:
            outline_color = YELLOW
        else:
            outline_color = WHITE
            
        pygame.draw.rect(canvas, outline_color, button_rect, 5)
        self.draw_centered_words(canvas)
        
    def draw_centered_words(self, canvas):
        if (self.selectable and self.hover) or self.selected:
            text_color = YELLOW
        elif self.selectable:
            text_color = WHITE
        else:
            text_color = GRAY
        
        msg_surf = self.font.render(self.text, True, text_color)
        msg_rect = msg_surf.get_rect()
        msg_rect.center = self.get_rect_pos().center
        canvas.blit(msg_surf, msg_rect)
    
