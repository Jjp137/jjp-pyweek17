import pygame
from pygame.locals import *

import data
import pickle
from constants import *

class Profile(object):
    def __init__(self):
        self.new_profile()

    def new_profile(self):
        # Options
        self.sound = True
        self.music = False
        
        # Stats
        self.current_health = 30
        self.max_health = 30
        self.current_fuel = 50
        self.max_fuel = 50
        self.money = 0
        self.speed = 2
        self.speed_level = 1
        
        # Equipment
        self.drill_level = 1
        self.scanner_level = 1
        self.machine_gun_level = 0
        self.spread_level = 0
        
        self.missile_count = 0
        self.missile_level = 0
        
        # Location state
        self.location_state = {"alpha-1" : UNVISITED,
                               "alpha-2" : UNVISITED,
                               "alpha-3" : UNVISITED,
                               "beta-1" : UNVISITED,
                               "beta-2" : UNVISITED,
                               "beta-3" : UNVISITED,
                               "beta-4" : UNVISITED,
                               "omega-1" : UNVISITED,
                               "omega-2" : UNVISITED}
        
        # Key items
        self.key_items = {A1_COORDS : True,
                          A2_COORDS : False,
                          A3_COORDS : False,
                          B1_COORDS : False,
                          B2_COORDS : False,
                          B3_COORDS : False,
                          B4_COORDS : False,
                          OM1_COORDS : False,
                          OM2_COORDS_A : False,
                          OM2_COORDS_B : False,
                          OM2_COORDS_C : False,
                          OM2_COORDS_D : False,
                          RED_CRYSTAL : False,
                          GUN_SCHEME : False,
                          SPREAD_SCHEME : False,
                          MISSILE_SCHEME : False}
        
        # Key flags
        self.did_tutorial = False
        self.site2_visit = False
        self.site3_ambush = False
        self.site4_visit = False

    def update(self, vehicle):
        self.current_health = vehicle.current_health
        self.current_fuel = vehicle.current_fuel
        self.money = vehicle.money
        self.missile_count = vehicle.equipment_actions[EQUIP_FIVE].count
        
        for thing in vehicle.key_inventory:
            self.key_items[thing] = True
        
    def save(self):
        f = open(data.savepath(), 'wb')
        pickle.dump(self, f)
        f.close()
