# Enemies

import math
import pygame
from pygame.locals import *

import entities
import random
import util
import data
from constants import *

class Enemy(object):
    def __init__(self, pos):
        raise NotImplementedError
        
    def update(self, player, terrain):
        raise NotImplementedError
        
    def move_pos(self, terrain):
        source_center = self.get_pos_rect().center
        move_step = util.velocity_vector(source_center, self.target, self.speed)
        
        x_move = move_step[X_INDEX]
        y_move = move_step[Y_INDEX]
        
        x_collision = False
        y_collision = False
        
        x_shift = [self.pos[X_INDEX] + x_move, self.pos[Y_INDEX]]
        while terrain.check_immobile_collision(self.get_pos_rect(x_shift)):
            x_collision = True
            
            if x_move > 0:
                x_move -= 1
                x_shift[X_INDEX] -= 1
            elif x_move < 0:
                x_move += 1
                x_shift[X_INDEX] += 1
        
        y_shift = [self.pos[X_INDEX], self.pos[Y_INDEX] + y_move]
        while terrain.check_immobile_collision(self.get_pos_rect(y_shift)):
            y_collision = True
            
            if y_move > 0:
                y_move -= 1
                y_shift[Y_INDEX] -= 1
            elif y_move < 0:
                y_move += 1
                y_shift[Y_INDEX] += 1
                
        # Check for both at the same time here
        both_shift = [self.pos[X_INDEX] + x_move, self.pos[Y_INDEX] + y_move]
        if not x_collision and not y_collision:
            while terrain.check_immobile_collision(self.get_pos_rect(both_shift)):
                if x_move > 0:
                    x_move -= 1
                    both_shift[X_INDEX] -= 1
                elif x_move < 0:
                    x_move += 1
                    both_shift[X_INDEX] += 1
                
                if y_move > 0:
                    y_move -= 1
                    both_shift[Y_INDEX] -= 1
                elif y_move < 0:
                    y_move += 1
                    both_shift[Y_INDEX] += 1
        
        self.pos[X_INDEX] += x_move
        self.pos[Y_INDEX] += y_move
        
    def on_damage(self, damage):
        raise NotImplementedError
        
    def draw(self, canvas, draw_pos=None):
        raise NotImplementedError
        
    def get_pos_rect(self, pos=None):
        raise NotImplementedError
        
    def get_hitbox(self, pos=None):
        raise NotImplementedError

class GreenThing(Enemy):
    def __init__(self, pos):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = pos[:]
        
        self.aware = False
        self.aware_timeout = 0
        self.idle_timeout = 0
        self.bullet_timeout = 0
        
        self.health = 20
        self.speed = 1.5
        
        self.image = util.load_alpha_image('green-enemy.png')
    
    def update(self, player, terrain):
        if self.health <= 0:
            return
        
        self.check_sight_range(player)
        
        enemy_center = self.get_pos_rect().center
        dist_from_target = util.distance_formula(enemy_center, self.target)

        if self.aware:
            self.target = player.get_pos_rect().center
        elif dist_from_target > 40 and self.idle_timeout > 0:
            self.target = self.start_pos
            self.idle_timeout -= 1
        elif self.idle_timeout <= 0:
            self.target[X_INDEX] = self.pos[X_INDEX] + random.randint(-100, 100)
            self.target[Y_INDEX] = self.pos[Y_INDEX] + random.randint(-100, 100)
            self.idle_timeout = random.randint(1, 3) * FPS
        else:
            self.idle_timeout -= 1
        
        if dist_from_target > 40:
            self.move_pos(terrain)
            
        if self.aware and self.bullet_timeout <= 0:
            self.fire_bullet(player, terrain)
            terrain.enemy_shoot_sound_play = True
        else:
            self.bullet_timeout -= 1
        
        if self.aware_timeout <= 0:
            self.aware = False
        else:
            self.aware_timeout -= 1
            
    def move_pos(self, terrain):
        super(GreenThing, self).move_pos(terrain)
            
    def check_sight_range(self, player):
        sight_rect = pygame.Rect(0, 0, 500, 500)
        sight_rect.center = self.get_pos_rect().center
        
        if sight_rect.contains(player.get_pos_rect()):
            self.aware = True
            self.aware_timeout = 5 * FPS
        
    def fire_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        source_pos = self.get_pos_rect().center
        
        bullet_vel = util.velocity_vector(source_pos, player_pos, 5)
        
        new_bullet = entities.Bullet(source_pos, bullet_vel, 2, DARKGREEN, False)
        terrain.bullets.append(new_bullet)
        
        self.bullet_timeout = 1 * FPS
    
    def on_damage(self, damage, player):
        self.health -= damage
        
        dist_from_target = util.distance_formula(player.get_pos_rect().center,
                                                 self.get_pos_rect().center)
        
        # Don't be awakened if the player is really far away
        if not self.aware and dist_from_target < 1000:
            self.aware = True
            self.aware_timeout = 5 * FPS
    
    def draw(self, canvas, draw_pos=None):
        if draw_pos == None:
            draw_pos = self.pos
            print "Warning: No draw_pos given."
        
        canvas.blit(self.image, draw_pos)
        
    def get_pos_rect(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        return pygame.Rect(pos[X_INDEX], pos[Y_INDEX], 32, 32)
        
    def get_hitbox(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        hitbox = pygame.Rect(pos[X_INDEX], pos[Y_INDEX], 32, 32)
        hitbox.center = self.get_pos_rect(pos).center
        
        return hitbox


class StrongGreenThing(GreenThing):
    def __init__(self, pos):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = pos[:]
        
        self.aware = False
        self.aware_timeout = 0
        self.idle_timeout = 0
        self.bullet_timeout = 0
        
        self.health = 90
        self.speed = 3
        
        self.image = util.load_alpha_image('strong-green-enemy.png')
        
    def fire_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        source_pos = self.get_pos_rect().center
        
        bullet_vel = util.velocity_vector(source_pos, player_pos, 6)
        
        new_bullet = entities.Bullet(source_pos, bullet_vel, 4, DARKGREEN, False)
        terrain.bullets.append(new_bullet)
        
        self.bullet_timeout = int(0.5 * FPS)


class YellowThing(GreenThing):
    def __init__(self, pos):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = pos[:]
        
        self.aware = False
        self.aware_timeout = 0
        self.idle_timeout = 0
        self.bullet_timeout = 0
        
        self.health = 35
        self.speed = 1
        
        self.image = util.load_alpha_image('yellow-enemy.png')
        
    def fire_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        source_pos = self.get_pos_rect().center
        
        center_angle = util.get_angle(source_pos, player_pos)
        angle_start = center_angle - (math.pi / 8) * (3/2)
        
        for i in range(3):
            bullet_angle = angle_start + (math.pi / 8) * i
            bullet_vel = util.velocity_with_angle(bullet_angle, 4)
            new_bullet = entities.Bullet(source_pos, bullet_vel, 2, ORANGE, False)
            
            terrain.bullets.append(new_bullet)
            
        if random.randint(1, 10) == 1:
            self.bullet_timeout = int(0.3 * FPS)
        else:
            self.bullet_timeout = int(1.5 * FPS)


class BlueThing(GreenThing):
    def __init__(self, pos):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = pos[:]
        
        self.aware = False
        self.aware_timeout = 0
        self.idle_timeout = 0
        self.bullet_timeout = 0
        
        self.health = 50
        self.speed = 1.25
        
        self.image = util.load_alpha_image('blue-enemy.png')
        
    def fire_bullet(self, player, terrain):
        source_pos = self.get_pos_rect().center
        
        bullet_angle = -math.pi
        while bullet_angle < math.pi:
            bullet_vel = util.velocity_with_angle(bullet_angle, 4)
            new_bullet = entities.Bullet(source_pos, bullet_vel, 3, PURPLE, False)
            
            terrain.bullets.append(new_bullet)
            bullet_angle += math.pi / 8
            
        self.bullet_timeout = int(1.5 * FPS)


class PinkThing(GreenThing):
    def __init__(self, pos):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = pos[:]
        
        self.aware = False
        self.aware_timeout = 0
        self.idle_timeout = 0
        
        self.bullet_timeout = 0
        self.fire_count = random.randint(3, 8)
        self.firing = False
        
        self.health = 75
        self.speed = 2.5
        
        self.image = util.load_alpha_image('pink-enemy.png')
        
    def update(self, player, terrain):
        if self.health <= 0:
            return
        
        self.check_sight_range(player)
        
        enemy_center = self.get_pos_rect().center
        dist_from_target = util.distance_formula(enemy_center, self.target)

        if self.aware:
            self.target = player.get_pos_rect().center
        elif dist_from_target > 40:
            self.target = self.start_pos
        elif self.idle_timeout <= 0:
            self.target[X_INDEX] += random.randint(-100, 100)
            self.target[Y_INDEX] += random.randint(-100, 100)
            self.idle_timeout = random.randint(1, 3) * FPS
        else:
            self.idle_timeout -= 1
        
        if dist_from_target > 40 and not self.firing:
            self.move_pos(terrain)
            
        if self.aware and self.bullet_timeout <= 0:
            self.firing = True
            self.fire_bullet(player, terrain)
            terrain.enemy_shoot_sound_play = True
        else:
            self.bullet_timeout -= 1
        
        if self.aware_timeout <= 0:
            self.aware = False
            self.bullet_timeout = 0
        else:
            self.aware_timeout -= 1
        
    def fire_bullet(self, player, terrain):
        player_pos = player.get_pos_rect().center
        source_pos = self.get_pos_rect().center
        
        for i in range(2):
            bullet_angle = util.get_angle(source_pos, player_pos)
            random_factor = math.pi / 8 * random.random()
            
            if random.randint(0, 1) == 0:
                bullet_angle += random_factor
            else:
                bullet_angle -= random_factor
            
            bullet_vel = util.velocity_with_angle(bullet_angle, 6)
            new_bullet = entities.Bullet(source_pos, bullet_vel, 2, PINK, False)
                
            terrain.bullets.append(new_bullet)
                   
        self.fire_count -= 1
        if self.fire_count <= 0:
            self.bullet_timeout = 2 * FPS
            self.fire_count = random.randint(3, 8)
            self.firing = False
        else:
            self.bullet_timeout = int(0.1 * FPS)


class BlackThing(GreenThing):
    def __init__(self, pos, boss=False):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = pos[:]
        self.boss_flag = boss
        
        self.aware = False
        self.aware_timeout = 0
        self.idle_timeout = 0
        
        self.bullet_timeout = 0
        self.fire_count = 10
        self.use_spread = random.choice([True, False])
        self.firing = False
        
        self.health = 140
        self.speed = 3.5
        
        self.image = util.load_alpha_image('black-enemy.png')
        
    def update(self, player, terrain):
        if self.health <= 0:
            return
        
        self.check_sight_range(player)
        
        enemy_center = self.get_pos_rect().center
        dist_from_target = util.distance_formula(enemy_center, self.target)

        if self.aware or self.boss_flag:
            self.target = player.get_pos_rect().center
        elif dist_from_target > 40:
            self.target = self.start_pos
        elif self.idle_timeout <= 0:
            self.target[X_INDEX] += random.randint(-100, 100)
            self.target[Y_INDEX] += random.randint(-100, 100)
            self.idle_timeout = random.randint(1, 3) * FPS
        else:
            self.idle_timeout -= 1
        
        self.adjust_speed(dist_from_target)
        
        if dist_from_target > 40 and not self.firing:
            self.move_pos(terrain)
            
        if self.aware and self.bullet_timeout <= 0:
            self.firing = True
            if self.use_spread:
                self.fire_spread(player, terrain)
            else:
                self.fire_stream(player, terrain)
                
            terrain.enemy_shoot_sound_play = True
        else:
            self.bullet_timeout -= 1
        
        if self.aware_timeout <= 0:
            self.aware = False
        else:
            self.aware_timeout -= 1
    
    def adjust_speed(self, dist):
        if dist > 600:
            self.speed = 5
        elif dist > 300:
            self.speed = 4
        else:
            self.speed = 3
        
    def fire_stream(self, player, terrain):
        player_pos = player.get_pos_rect().center
        source_pos = self.get_pos_rect().center
        
        bullet_vel = util.velocity_vector(source_pos, player_pos, 8)
        new_bullet = entities.Bullet(source_pos, bullet_vel, 2, BLACK, False)
            
        terrain.bullets.append(new_bullet)
                   
        self.fire_count -= 1
        if self.fire_count <= 0:
            self.bullet_timeout = int((1 * random.random() + 0.5) * FPS)
            self.fire_count = 10
            self.firing = False
            self.use_spread = random.choice([True, False])
        else:
            self.bullet_timeout = int(0.1 * FPS)
            
    def fire_spread(self, player, terrain):
        player_pos = player.get_pos_rect().center
        source_pos = self.get_pos_rect().center
        
        center_angle = util.get_angle(source_pos, player_pos)
        angle_start = center_angle - (math.pi / 8) * (3/2)
        
        for i in range(3):
            bullet_angle = angle_start + (math.pi / 8) * i
            bullet_vel = util.velocity_with_angle(bullet_angle, 8)
            new_bullet = entities.Bullet(source_pos, bullet_vel, 2, BLACK, False)
            
            terrain.bullets.append(new_bullet)
            
        self.fire_count -= 2
        if self.fire_count <= 0:
            self.bullet_timeout = int((1 * random.random() + 0.5) * FPS)
            self.fire_count = 10
            self.firing = False
            self.use_spread = random.choice([True, False])
        else:
            self.bullet_timeout = int(0.2 * FPS)

class Boss(Enemy):
    def __init__(self, pos):
        self.start_pos = pos[:]
        self.pos = pos[:]
        self.target = None
        
        self.health = 3100
        self.speed = 0.2
        
        self.spawn_timeout = 0
        self.berserk_timeout = 0
        self.berserk_density = 1
        
        self.image = util.load_alpha_image('the-boss.png')
        
    def update(self, player, terrain):
        self.target = player.get_pos_rect()
        
        self.move_pos(terrain)
        
        if self.spawn_timeout <= 0:
            self.spawn_enemy(terrain)
        else:
            self.spawn_timeout -= 1
            
        if self.berserk_timeout == 0:
            self.berserk_bullet(terrain)
            self.berserk_timeout = 3
            terrain.enemy_shoot_sound_play = True
        else:
            self.berserk_timeout -= 1
        
    def spawn_enemy(self, terrain):
        spawn_pos = list(self.get_pos_rect().center)
        terrain.enemies.insert(0, BlackThing(spawn_pos, True))
        self.spawn_timeout = 30 * FPS
        
    def berserk_bullet(self, terrain):
        for i in range(self.berserk_density):
            source_pos = self.get_pos_rect().center
            random_angle = math.pi * random.random()
                
            if random.randint(0, 1) == 0:
                random_angle = -random_angle
            
            bullet_vel = util.velocity_with_angle(random_angle, 4)
            new_bullet = entities.Bullet(source_pos, bullet_vel, 1, RED, False)
            terrain.bullets.append(new_bullet)
    
    def on_damage(self, damage, player):
        self.health -= damage
        
        if self.health <= 500:
            self.berserk_density = 8
        elif self.health <= 1000:
            self.berserk_density = 6
        elif self.health <= 1500:
            self.berserk_density = 4
        elif self.health <= 2000:
            self.berserk_density = 3
        elif self.health <= 2500:
            self.berserk_density = 2
        
    def draw(self, canvas, draw_pos=None):
        if draw_pos == None:
            draw_pos = self.pos
            print "Warning: No draw_pos given."
        
        canvas.blit(self.image, draw_pos)
        
    def get_pos_rect(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        return pygame.Rect(pos[X_INDEX], pos[Y_INDEX], 512, 410)
        
    def get_hitbox(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        hitbox = pygame.Rect(pos[X_INDEX] + 75, pos[Y_INDEX] + 65, 382, 325)
        
        return hitbox
