import pygame
from pygame.locals import *

import data
import util
from constants import *

class StatusBar(object):
    def __init__(self, terrain):
        self.small_font = pygame.font.Font(data.filepath('xolonium-regular.ttf'), 16)
        self.player = terrain.player_vehicle
        
        # Images for status icons
        self.drill_image = pygame.image.load(data.filepath("icon-drill.png")).convert()
        self.scanner_image = pygame.image.load(data.filepath("icon-scanner.png")).convert()
        self.gun_image = pygame.image.load(data.filepath("icon-gun.png")).convert()
        self.spread_image = pygame.image.load(data.filepath("icon-spread.png")).convert()
        self.missile_image = pygame.image.load(data.filepath("icon-missile.png")).convert()
        self.equip_outline = util.load_alpha_image("icon-outline.png")
        
        self.image_array = [self.drill_image, self.scanner_image, self.gun_image, 
            self.spread_image, self.missile_image]
        
        self.obtain_player_info()
        
    def update(self, fps):
        self.obtain_player_info()
        self.fps = int(fps)
        
    def obtain_player_info(self):
        self.current_health = self.player.current_health
        self.max_health = self.player.max_health
        self.current_fuel = self.player.current_fuel
        self.max_fuel = self.player.max_fuel
        self.money = self.player.money
        
        self.equipped_num = self.player.current_equip_id
        self.equipped_info = self.player.current_equip.get_status_info()
        self.equips_to_draw = [False, False, False, False, False]
        
        for equip_key in self.player.equipment_actions.keys():
            if self.player.equipment_actions[equip_key].level >= 1:
                self.equips_to_draw[equip_key - 11] = True
        
    def draw(self, canvas):
        self.draw_bg(canvas)
        self.draw_equip_icons(canvas)
        self.draw_equip_info(canvas)
        self.draw_bars(canvas)
        self.draw_fps(canvas)
        
    def draw_bg(self, canvas):
        status_size = (WINDOW_WIDTH, STATUS_HEIGHT)
        
        status_surface = pygame.Surface(status_size).convert_alpha()
        status_surface.fill(TRANS_BLACK)
        status_rect = status_surface.get_rect()
        status_rect.topleft = (0, 550)
        
        canvas.blit(status_surface, status_rect)
        
    def draw_equip_icons(self, canvas):
        for i in range(len(self.image_array)):
            draw_pos = (20 + (60 * i), 560)
            
            if self.equips_to_draw[i]:
                canvas.blit(self.image_array[i], draw_pos)
        
        canvas.blit(self.equip_outline, (20 + (self.equipped_num - 1) * 60, 560))
        
    def draw_equip_info(self, canvas):
        self.draw_single_bar(canvas, (20, 615), self.equipped_info[1], self.equipped_info[2], DARKGREEN, 300)
        
        equip_name = self.equipped_info[0]
        self.draw_words(canvas, "Equipped: " + equip_name, (20, 610))
        
    def draw_bars(self, canvas):
        self.draw_words(canvas, "Health:", (500, 560))
        self.draw_single_bar(canvas, (570, 565), self.current_health, self.max_health, BLUE)
        self.draw_words(canvas, str(self.current_health) + "/" + str(self.max_health), (680, 560))
        
        self.draw_words(canvas, "Fuel:", (500, 585))
        self.draw_single_bar(canvas, (570, 590), self.current_fuel, self.max_fuel, ORANGE)
        self.draw_words(canvas, str(self.current_fuel) + "/" + str(self.max_fuel), (680, 585))
        
        self.draw_words(canvas, "Money: " + str(self.money), (500, 610))
        
    def draw_fps(self, canvas):
        self.draw_words(canvas, "FPS: " + str(self.fps), (720, 630))
        
    def draw_words(self, canvas, string, topleft, small=True):
        msg_surf = self.small_font.render(string, True, WHITE)
        msg_rect = msg_surf.get_rect()
        msg_rect.topleft = topleft
        canvas.blit(msg_surf, msg_rect)
        
    def draw_single_bar(self, canvas, topleft, current, maximum, color, max_length=100):
        filled_length = int((1.0 * current / maximum) * max_length)
        if filled_length <= 0:
            return # There's no bar to draw

        pygame.draw.rect(canvas, color, (topleft[X_INDEX], topleft[Y_INDEX], filled_length, 12))
        
