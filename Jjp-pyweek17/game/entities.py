# (Most) game objects are in this file

import pygame
from pygame.locals import *

import util
import data
from constants import *

class Barrier(object):
    def __init__(self, orientation, pos=None, length=0):
        if pos == None:
            self.pos = [0, 0]
        else:
            self.pos = pos[:]
            
        self.orientation = orientation
        if self.orientation == HORIZONTAL:
            self.image = util.load_alpha_image("horiz_barrier.png")
        elif self.orientation == VERTICAL:
            self.image = util.load_alpha_image("vert_barrier.png")
        else:
            raise ValueError
        
        self.length = length

    def draw(self, canvas, draw_pos=None):
        if draw_pos == None:
            draw_pos = self.pos[:]
            print "Warning: no draw_pos was given."
        
        if self.orientation == HORIZONTAL:
            for x_offset in xrange(0, self.length, BARRIERSIZE):
                canvas.blit(self.image, (draw_pos[X_INDEX] + x_offset, draw_pos[Y_INDEX]))
        elif self.orientation == VERTICAL:
            for y_offset in xrange(0, self.length, BARRIERSIZE):
                canvas.blit(self.image, (draw_pos[X_INDEX], draw_pos[Y_INDEX] + y_offset))
        
    def get_pos_rect(self):
        if self.orientation == HORIZONTAL:
            return pygame.Rect(self.pos[X_INDEX], self.pos[Y_INDEX], 
                self.length, BARRIERSIZE)
        elif self.orientation == VERTICAL:
            return pygame.Rect(self.pos[X_INDEX], self.pos[Y_INDEX], 
                BARRIERSIZE, self.length)


class Rock(object):
    def __init__(self, pos=None):
        if pos == None:
            self.pos = [0, 0]
        else:
            self.pos = pos[:]
            
        self.image = util.load_alpha_image("rock.png")
        
    def draw(self, canvas, draw_pos=None):
        if draw_pos == None:
            draw_pos = self.pos[:]
            print "Warning: no draw_pos was given."
            
        canvas.blit(self.image, draw_pos)
        
    def get_pos_rect(self):
        return pygame.Rect(self.pos[X_INDEX], 
            self.pos[Y_INDEX], ROCKSIZE, ROCKSIZE)


class Teleporter(object):
    def __init__(self, pos=None):
        if pos == None:
            self.pos = [0, 0]
        else:
            self.pos = pos[:]
            
        self.image = pygame.image.load(data.filepath("teleporter.png")).convert()
            
    def draw(self, canvas, draw_pos=None):
        if draw_pos == None:
            draw_pos = self.pos[:]
            print "Warning: no draw_pos was given."
            
        canvas.blit(self.image, draw_pos)
        
    def get_pos_rect(self):
        return pygame.Rect(self.pos[X_INDEX], 
            self.pos[Y_INDEX], TELEPORTSIZE, TELEPORTSIZE)


class DigSite(object):
    def __init__(self, pos, item):
        self.item = item
        self.key_item = False
        self.scanned = False
        self.scan_timeout = 0
        self.drilled = False
        
        self.pos = pos[:]
            
    def reveal(self, timeout):
        self.scanned = True
        self.scan_timeout = timeout
            
    def update(self):
        if self.scan_timeout > 0:
            self.scan_timeout -= 1
        elif self.scan_timeout == 0:
            self.scanned = False
            
    def draw(self, canvas, draw_pos=None):
        if not self.scanned or self.drilled:
            return
        
        if draw_pos == None:
            draw_pos = self.pos[:]
            print "Warning: no draw_pos was given."
            
        digsite_surface = pygame.Surface((DIGSIZE, DIGSIZE)).convert_alpha()
        digsite_surface.fill(TRANS_YELLOW if self.key_item else TRANS_GREEN)
        digsite_rect = digsite_surface.get_rect()
        digsite_rect.topleft = tuple(draw_pos)
        
        canvas.blit(digsite_surface, digsite_rect)
        
    def get_pos_rect(self):
        return pygame.Rect(self.pos[X_INDEX], self.pos[Y_INDEX], DIGSIZE, DIGSIZE)


class Bullet(object):
    def __init__(self, pos, velocity, damage, color, friendly=False, size=8, sprite=None):
        self.pos = list(pos)
        self.velocity = velocity
        self.friendly = friendly
        self.damage = damage
        self.hit = False
        
        self.size = size
        self.color = color
        self.sprite = sprite
        
    def update(self):
        self.pos[X_INDEX] += self.velocity[X_INDEX]
        self.pos[Y_INDEX] += self.velocity[Y_INDEX]
        
    def draw(self, canvas, draw_pos=None):
        if self.sprite == None:
            pygame.draw.rect(canvas, self.color, self.get_pos_rect(draw_pos))
        
    def get_pos_rect(self, pos=None):
        if pos == None:
            pos = self.pos[:]
        
        return pygame.Rect(pos[X_INDEX], pos[Y_INDEX], self.size, self.size)
