import pygame
import sys
from pygame.locals import *

from constants import *

class MoonMap(object):
    def __init__(self, profile):
        self.profile = profile
        self.destinations = []
        self.connectors = []
        self.selected_dest = None
        
        self.build_map()
        
    def build_map(self):
        key_items = self.profile.key_items
        
        if key_items[A1_COORDS]:
            state = self.profile.location_state["alpha-1"]
            new_dest = Destination((200, 200), "Alpha Site 1", "site1.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[A2_COORDS]:
            self.connectors.append(Connector((240, 220), (280, 220)))
            
            state = self.profile.location_state["alpha-2"]
            new_dest = Destination((280, 200), "Alpha Site 2", "site2.txt", state)
            self.destinations.append(new_dest)
        
        if key_items[A3_COORDS]:
            self.connectors.append(Connector((320, 220), (360, 220)))
            
            state = self.profile.location_state["alpha-3"]
            new_dest = Destination((360, 200), "Alpha Site 3", "site3.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[B1_COORDS]:
            self.connectors.append(Connector((400, 220), (440, 220)))
            
            state = self.profile.location_state["beta-1"]
            new_dest = Destination((440, 200), "Beta Site 1", "site4.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[B2_COORDS]:
            self.connectors.append(Connector((480, 220), (520, 220)))
            
            state = self.profile.location_state["beta-2"]
            new_dest = Destination((520, 200), "Beta Site 2", "site5.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[B3_COORDS]:
            self.connectors.append(Connector((540, 240), (540, 280)))
            
            state = self.profile.location_state["beta-3"]
            new_dest = Destination((520, 280), "Beta Site 3", "site6.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[B4_COORDS]:
            self.connectors.append(Connector((480, 300), (520, 300)))
            
            state = self.profile.location_state["beta-4"]
            new_dest = Destination((440, 280), "Beta Site 4", "site7.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[OM1_COORDS]:
            self.connectors.append(Connector((460, 320), (460, 360)))
            
            state = self.profile.location_state["omega-1"]
            new_dest = Destination((440, 360), "Omega Site 1", "site8.txt", state)
            self.destinations.append(new_dest)
            
        if key_items[OM2_COORDS_A] and key_items[OM2_COORDS_B] \
            and key_items[OM2_COORDS_C] and key_items[OM2_COORDS_D]:
            self.connectors.append(Connector((400, 380), (440, 380)))
            
            state = self.profile.location_state["omega-2"]
            new_dest = Destination((360, 360), "Omega Site 2", "site9.txt", state)
            self.destinations.append(new_dest)
        
    def read_pointer(self, pointer):
        for dest in self.destinations:
            dest.read_pointer(pointer)
        
    def read_input(self, event):
        for dest in self.destinations:
            was_selected = dest.selected
            
            dest.read_input(event)
            if not was_selected and dest.selected:
                self.selected_dest = dest
        
        for dest in self.destinations:
            if dest != self.selected_dest:
                dest.selected = False
        
    def draw(self, canvas):
        for dest in self.destinations:
            dest.draw(canvas)
        
        for line in self.connectors:
            line.draw(canvas)
            
    def dest_name(self):
        if self.selected_dest == None:
            return "Select a destination."
        else:
            return self.selected_dest.dest

class Destination(object):
    def __init__(self, pos, dest, filename, state):
        self.pos = pos
        self.dest = dest
        self.filename = filename
        self.state = state
        
        self.hover = False
        self.clicked = False
        self.selected = False
        
    def read_pointer(self, pointer):
        if self.get_rect_pos().collidepoint(pointer):
            self.hover = True
        else:
            self.hover = False
        
    def read_input(self, event):
        if event.type == MOUSEBUTTONDOWN and event.button == 1 and self.hover:
            self.clicked = True
        if event.type == MOUSEBUTTONUP and event.button == 1 \
            and self.hover and self.clicked:
            self.clicked = False
            self.selected = True
    
    def draw(self, canvas):
        dest_rect = self.get_rect_pos()
        
        color = self.get_color()
        pygame.draw.rect(canvas, color, dest_rect)
        
    def get_color(self):
        if self.hover:
            return YELLOW
        elif self.selected:
            return ORANGE
        elif self.state == UNVISITED:
            return WHITE
        elif self.state == NOT_COMPLETE:
            return RED
        elif self.state == COMPLETE:
            return BLUE
        
    def get_rect_pos(self):
        return pygame.Rect(self.pos[X_INDEX], self.pos[Y_INDEX], 40, 40)

class Connector(object):
    def __init__(self, start, end):
        self.start_pos = start
        self.end_pos = end
        
    def draw(self, canvas):
        pygame.draw.line(canvas, WHITE, self.start_pos, self.end_pos, 3)
