# Random functions that can be used in a lot of places

import math
import pygame
from pygame.locals import *

import data
from constants import *

def load_alpha_image(filename):
    return pygame.image.load(data.filepath(filename)).convert_alpha()

def distance_formula(first_pos, second_pos):
    x_diff = first_pos[X_INDEX] - second_pos[X_INDEX]
    y_diff = first_pos[Y_INDEX] - second_pos[Y_INDEX]
    
    return math.sqrt(x_diff ** 2 + y_diff ** 2)
    
def velocity_vector(source_pos, target_pos, speed):
    angle = get_angle(source_pos, target_pos)

    return velocity_with_angle(angle, speed)
    
def velocity_with_angle(angle, speed):
    x_vel = speed * math.cos(angle)
    y_vel = speed * math.sin(angle)
    
    return (x_vel, y_vel)
    
def get_angle(source_pos, target_pos):
    x_diff = target_pos[X_INDEX] - source_pos[X_INDEX]
    y_diff = target_pos[Y_INDEX] - source_pos[Y_INDEX]
    
    return math.atan2(y_diff, x_diff)
